<?php

namespace Lib\Model;

use Lib\Model\Base\FinanceTransactionVersion as BaseFinanceTransactionVersion;

/**
 * Skeleton subclass for representing a row from the 'Finance__Transaction_version' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FinanceTransactionVersion extends BaseFinanceTransactionVersion
{

}
