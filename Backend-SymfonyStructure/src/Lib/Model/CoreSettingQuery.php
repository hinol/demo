<?php

namespace Lib\Model;

use Lib\Model\Base\CoreSettingQuery as BaseCoreSettingQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Core__Setting' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CoreSettingQuery extends BaseCoreSettingQuery
{

}
