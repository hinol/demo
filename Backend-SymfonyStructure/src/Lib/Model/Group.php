<?php

namespace Lib\Model;

use FOS\UserBundle\Model\GroupInterface;

/**
 * Class Group
 * @package Lib\Model
 */
class Group extends \Lib\Model\Base\Group implements GroupInterface
{

    /**
     * @return int
     */
    public function userCnt(){
        return $this->getUserGroups()->count();
    }
}