<?php

namespace Lib\Model;

use Lib\Model\Base\UserData as BaseUserData;
use Lib\Model\Map\UserDataTableMap;
use Lib\Model\Map\UserTableMap;

/**
 * Class UserData
 * @package Lib\Model
 */
class UserData extends BaseUserData
{
    /**
     * @return bool
     * @throws \PropelException
     */
    public function isPhoneActive()
    {
        if ($this->getPhoneStatus() == UserDataTableMap::COL_PHONE_STATUS_ACTIVE) {
            return true;
        }
        return false;
    }

    /**
     * @param string $v
     * @return UserData
     */
    public function setPhoneCode($v)
    {
        $this->setPhoneCodeAt(new \DateTime());
        return parent::setPhoneCode($v);
    }
}
