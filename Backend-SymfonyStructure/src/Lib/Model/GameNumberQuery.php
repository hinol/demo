<?php

namespace Lib\Model;

use Lib\Model\Base\GameNumberQuery as BaseGameNumberQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Bet__GameNumber' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GameNumberQuery extends BaseGameNumberQuery
{

}
