<?php

namespace Lib\Model;

use Lib\Model\Base\UserUserdataArchive as BaseUserUserdataArchive;

/**
 * Skeleton subclass for representing a row from the 'User__UserData_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserUserdataArchive extends BaseUserUserdataArchive
{

}
