<?php

namespace Lib\Model;

use Lib\Model\Base\UserDataVersionQuery as BaseUserDataVersionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'User__UserData_version' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserDataVersionQuery extends BaseUserDataVersionQuery
{

}
