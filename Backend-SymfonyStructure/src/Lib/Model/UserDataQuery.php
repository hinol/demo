<?php

namespace Lib\Model;

use Lib\Model\Base\UserDataQuery as BaseUserDataQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'User__UserData' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserDataQuery extends BaseUserDataQuery
{

}
