<?php

namespace Lib\Model;

use Lib\Model\Base\CoreSettingVersionQuery as BaseCoreSettingVersionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Core__Setting_version' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CoreSettingVersionQuery extends BaseCoreSettingVersionQuery
{

}
