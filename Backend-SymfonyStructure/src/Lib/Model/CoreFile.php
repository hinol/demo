<?php

namespace Lib\Model;

use Lib\Model\Base\CoreFile as BaseCoreFile;

/**
 * Skeleton subclass for representing a row from the 'Core__File' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CoreFile extends BaseCoreFile
{

}
