<?php

namespace Lib\Model;

use Lib\Model\Base\FinanceTransactionQuery as BaseFinanceTransactionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Finance__Transaction' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FinanceTransactionQuery extends BaseFinanceTransactionQuery
{

}
