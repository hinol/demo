<?php

namespace Lib\Model;

use Lib\Model\Base\CoreSetting as BaseCoreSetting;

/**
 * Class CoreSetting
 * @package Lib\Model
 */
class CoreSetting extends BaseCoreSetting
{


    /**
     * @param string $v
     * @return CoreSetting
     */
    public function setValue($v)
    {
        
        $this->setValueType(gettype($v));
        return parent::setValue($v);
    }


    /**
     * @return bool|float|int|string
     * @throws \Exception
     */
    public function getValue()
    {

        switch ($this->getValueType()) {
            case "float":
                return (float)$this->value;
            case "double":
                return (float)$this->value;
            case "integer":
                return (int)$this->value;
            case "boolean":
                return (bool)$this->value;
            case "string":
                return (string)$this->value;

        }

        return $this->value;
        throw new \Exception("Bad Value type" . $this->getValueType());
    }

}




