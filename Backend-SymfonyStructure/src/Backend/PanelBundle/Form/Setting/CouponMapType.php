<?php


namespace Backend\PanelBundle\Form\Setting;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CouponMapType
 * @package Backend\PanelBundle\Form\Setting
 */
class CouponMapType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add("coupon_width", "number");
        $builder->add("coupon_height", "number");

        for ($i = 1; $i <= 46; $i++) {
            $builder->add($i, "setting_coupon_map_sub");
        }

        $builder->add("system7", "setting_coupon_map_sub");
        $builder->add("system8", "setting_coupon_map_sub");
        $builder->add("system9", "setting_coupon_map_sub");
        $builder->add("system10", "setting_coupon_map_sub");
        $builder->add("system11", "setting_coupon_map_sub");
        $builder->add("system12", "setting_coupon_map_sub");
    }


    /** @inheritdoc */
    public function getName()
    {
        return "setting_coupon_map";
    }
}