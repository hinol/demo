<?php

namespace Backend\PanelBundle\Form\Setting;

use App\ModelBundle\Services\ConstService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class GameCalendarType
 * @package Backend\PanelBundle\Form\Setting
 */
class GameCalendarType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(ConstService::MINI_LOTTO, "setting_calendar_game_sub");
        $builder->add(ConstService::LOTTO, "setting_calendar_game_sub");
    }


    /** @inheritdoc */
    public function getName()
    {
        return "setting_calendar_game";
    }
}