<?php


namespace Backend\PanelBundle\Form\Setting;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class AutoPlayerType
 * @package Backend\PanelBundle\Form\Setting
 */
class AutoPlayerType extends AbstractType
{

    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $choices = [
            '1x3' => '1-3',
            '3x6' => '3-6',
            '6x7' => '6-7',
            '7x10' => '7-10',
        ];

        $builder
            ->add("user_id", "number")
            ->add("firstname", "text")
            ->add("lastname", "text")
            ->add("frequency", 'choice', [
                'choices' => [
                    'random10x120' => "random10x120",
                    '20' => '20',
                    '60' => '60',
                    '90' => '90',
                ]
            ])
            ->add("share", "choice", [
                'choices' => range(1, 20, 1)
            ])
            ->add("logickTo10", 'choice', ['choices' => $choices])
            ->add("logickTo11x30", 'choice', ['choices' => $choices])
            ->add("logickTo31x60", 'choice', ['choices' => $choices])
            ->add("logickTo61x90", 'choice', ['choices' => $choices])
            ->add("logickTo91x95", 'choice', ['choices' => $choices]);
    }


    /** @inheritdoc */
    public function getName()
    {
        return "setting_auto_player";
    }
}