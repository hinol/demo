<?php


namespace Backend\PanelBundle\Form\Setting;


use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CouponCloseType
 * @package Backend\PanelBundle\Form\Setting
 */
class CouponCloseType extends AbstractType
{

    /**
     * @var CoreSettingService
     */
    private $coreSettingService;

    /**
     * @param CoreSettingService $coreSettingService
     */
    public function __construct(CoreSettingService $coreSettingService)
    {
        $this->coreSettingService = $coreSettingService;
    }

    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(ConstService::MINI_LOTTO, "choice", ['choices' => $this->coreSettingService->getHoursRange()]);
        $builder->add(ConstService::LOTTO, "choice", ['choices' => $this->coreSettingService->getHoursRange()]);
    }


    /** @inheritdoc */
    public function getName()
    {
        return "setting_coupon_close";
    }
}