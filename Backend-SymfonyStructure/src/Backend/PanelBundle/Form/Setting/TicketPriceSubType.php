<?php


namespace Backend\PanelBundle\Form\Setting;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class TicketPriceSubType
 * @package Backend\PanelBundle\Form\Setting
 */
class TicketPriceSubType extends AbstractType
{


    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("7", "number", ['required' => false, 'attr'=>['size'=>4]])
            ->add("8", "number", ['required' => false, 'attr'=>['size'=>4]])
            ->add("9", "number", ['required' => false, 'attr'=>['size'=>4]])
            ->add("10", "number", ['required' => false, 'attr'=>['size'=>4]])
            ->add("11", "number", ['required' => false, 'attr'=>['size'=>4]])
            ->add("12", "number", ['required' => false, 'attr'=>['size'=>4]]);

    }


    /** @inheritdoc */
    public function getName()
    {
        return "setting_ticket_price_sub";
    }
}