<?php


namespace Backend\PanelBundle\Form\Setting;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class CouponMapSubType
 * @package Backend\PanelBundle\Form\Setting
 */
class CouponMapSubType extends AbstractType
{


    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $setting = ['required'=>false, 'attr'=>['size'=>4]];

        $builder
            ->add("x", "number", $setting)
            ->add("y", "number", $setting)
            ->add("width", "number", $setting)
            ->add("length", "number", $setting);
    }


    /** @inheritdoc */
    public function getName()
    {
        return "setting_coupon_map_sub";
    }
}