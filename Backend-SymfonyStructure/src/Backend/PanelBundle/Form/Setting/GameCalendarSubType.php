<?php


namespace Backend\PanelBundle\Form\Setting;

use App\ModelBundle\Services\Core\CoreSettingService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class GameCalendarSubType
 * @package Backend\PanelBundle\Form\Setting
 */
class GameCalendarSubType extends AbstractType
{

    /**
     * @var CoreSettingService
     */
    private $coreSettingService;

    /**
     * @param CoreSettingService $coreSettingService
     */
    public function __construct(CoreSettingService $coreSettingService)
    {
        $this->coreSettingService = $coreSettingService;
    }


    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("1", "checkbox", ['required' => false])
            ->add("2", "checkbox", ['required' => false])
            ->add("3", "checkbox", ['required' => false])
            ->add("4", "checkbox", ['required' => false])
            ->add("5", "checkbox", ['required' => false])
            ->add("6", "checkbox", ['required' => false])
            ->add("7", "checkbox", ['required' => false])
            ->add("hour", "choice", [
                'choices' => $this->coreSettingService->getHoursRange(),
                'preferred_choices' => ["20:00"]
            ]);
    }


    /** @inheritdoc */
    public function getName()
    {
        return "setting_calendar_game_sub";
    }
}