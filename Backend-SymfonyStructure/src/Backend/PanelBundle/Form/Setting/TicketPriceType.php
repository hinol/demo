<?php


namespace Backend\PanelBundle\Form\Setting;

use App\ModelBundle\Services\ConstService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class TicketPriceType
 * @package Backend\PanelBundle\Form\Setting
 */
class TicketPriceType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(ConstService::MINI_LOTTO, "setting_ticket_price_sub");
        $builder->add(ConstService::LOTTO, "setting_ticket_price_sub");
    }


    /** @inheritdoc */
    public function getName()
    {
        return "setting_ticket_price";
    }
}