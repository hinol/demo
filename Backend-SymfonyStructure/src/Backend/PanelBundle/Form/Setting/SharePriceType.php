<?php


namespace Backend\PanelBundle\Form\Setting;

use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class SharePriceType
 * @package Backend\PanelBundle\Form\Setting
 */
class SharePriceType extends AbstractType
{

    /**
     * @var CoreSettingService
     */
    private $coreSettingService;

    /**
     * @param CoreSettingService $coreSettingService
     */
    public function __construct(CoreSettingService $coreSettingService)
    {
        $this->coreSettingService = $coreSettingService;
    }

    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add("commision", "choice", [
                    'choices' => $this->coreSettingService->getShareRange()
                ]
            )
            ->add("commision_button_ceil", "checkbox", ['required' => false])
            ->add("share_count", "number", ['required' => false])
            ->add(ConstService::MINI_LOTTO, "setting_ticket_price_sub")
            ->add(ConstService::LOTTO, "setting_ticket_price_sub")
            ->add("commision_recount", "submit", ['attr'=>['class'=>'btn btn-success']]);
    }


    /** @inheritdoc */
    public function getName()
    {
        return "setting_share_price";
    }
}