<?php


namespace Backend\PanelBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class PanelMenuBuilder
 * @package Backend\PanelBundle\Menu
 */
class PanelMenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;
    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param FactoryInterface $factory
     * @param AuthorizationChecker $authorizationChecker
     * @param RequestStack $requestStack
     */
    public function __construct(FactoryInterface $factory, AuthorizationChecker $authorizationChecker, RequestStack $requestStack)
    {
        $this->factory = $factory;
        $this->authorizationChecker = $authorizationChecker;
        $this->requestStack = $requestStack;
    }

    /**
     * @return \Knp\Menu\ItemInterface
     */
    public function createPanelMenu()
    {
        $route = $this->requestStack->getCurrentRequest()->get("_route");

        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'sidebar-menu');
        $menu->setChildrenAttribute('data-auto-speed', '200');
        $menu->setChildrenAttribute('data-auto-scroll', 'false');


        $menu->addChild('games-management', array('route' => 'panel_game'))
            ->setAttribute("class", "treeview")
            ->setAttribute("arrow", 1)
            ->setChildrenAttribute("class", "treeview-menu")
            ->setAttribute('icon', 'money');
        $menu['games-management']->addChild("management", ['route' => 'panel_game']);
        $menu['games-management']->addChild("wins", ['route' => 'panel_game_wins']);


        $menu->addChild('bets-management', array('route' => 'backend_bet_manage'))
            ->setAttribute("class", "treeview")
            ->setAttribute("arrow", 1)
            ->setChildrenAttribute("class", "treeview-menu")
            ->setAttribute('icon', 'money');
        $menu['bets-management']->addChild("management", ['route' => 'backend_bet_manage']);
        $menu['bets-management']->addChild("wins", ['route' => 'panel_index']);
        $menu['bets-management']->addChild("wins-bets", ['route' => 'panel_index']);
        $menu['bets-management']->addChild("wins-summary", ['route' => 'panel_index']);


        $menu->addChild('users-management', array('route' => 'panel_user'))
            ->setAttribute("class", "treeview")
            ->setAttribute("arrow", 1)
            ->setChildrenAttribute("class", "treeview-menu")
            ->setAttribute('icon', 'money');

        $menu['users-management']->addChild("management", ['route' => 'panel_user']);
        $menu['users-management']->addChild("money-requests", ['route' => 'panel_user_request']);


        $menu->addChild('admins-management', array('route' => 'panel_admin_list'))
            ->setAttribute("class", "treeview")
            ->setAttribute("arrow", 1)
            ->setChildrenAttribute("class", "treeview-menu")
            ->setAttribute('icon', 'money');

        $menu['admins-management']->addChild("list", ['route' => 'panel_admin_list']);

        $menu['admins-management']->addChild("groups", ['route' => 'panel_admin_group']);


        $menu->addChild('system-settings', ['route' => 'panel_game_calendar', 'attr'=>['class'=>'treeview']])
            ->setAttribute("class", "treeview")
            ->setAttribute("arrow", 1)
            ->setChildrenAttribute("class", "treeview-menu")
            ->setAttribute('icon', 'money');

        $menu['system-settings']->addChild("calendar-events", ['route' => 'panel_game_calendar']);
        $menu['system-settings']->addChild("ticket-prices", ['route' => 'panel_ticket_price']);
        $menu['system-settings']->addChild("share-prices", ['route' => 'panel_share_price']);
        $menu['system-settings']->addChild("coupon-close", ['route' => 'panel_coupon_close']);
        $menu['system-settings']->addChild("coupon-map", ['route' => 'panel_coupon_map']);
        $menu['system-settings']->addChild("auto-player", ['route' => 'panel_auto_player']);
        $menu['system-settings']->addChild("misc", ['route' => 'panel_misc']);


        $menu->addChild('panel_test', ['route' => 'panel_test', 'attr'=>['class'=>'treeview']])
            ->setAttribute("class", "treeview")
            ->setAttribute("arrow", 1)
            ->setChildrenAttribute("class", "treeview-menu")
            ->setAttribute('icon', 'money');

        $menu['panel_test']->addChild("panel_test_run", ['route' => 'panel_test']);




        return $menu;
    }
}