<?php


namespace Backend\PanelBundle\Menu;


use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Router;

/**
 * Class PanelMenuVoter
 * @package Backend\PanelBundle\Menu
 */
class PanelMenuVoter implements VoterInterface
{


    /**
     * @var RequestStack
     */
    private $requestStack;


    /**
     * @var Router
     */
    private $router;

    /**
     * @param RequestStack $requestStack
     * @param Router $router
     */
    public function __construct(RequestStack $requestStack, Router $router)
    {
        $this->requestStack = $requestStack;
        $this->router = $router;
    }

    /**
     * @param ItemInterface $item
     * @return bool|null
     */
    public function matchItem(ItemInterface $item)
    {
        if($item->getUri() == "#"){
            return null;
        }

        if($item->getUri() == $this->requestStack->getCurrentRequest()->getRequestUri()){
            return true;
        }

        $itemRoute = $this->router->match($item->getUri());
        if($itemRoute){
            $itemRoute = $itemRoute['_route'];
            $currentRoute = $this->requestStack->getCurrentRequest()->get("_route");
            if(preg_match('/'.$itemRoute.'/', $currentRoute)){
                return true;
            }
        }
        return null;
    }
}