<?php

namespace Backend\PanelBundle\Controller;

use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class TicketPriceController
 * @package Backend\PanelBundle\Controller
 * @Route("/ticket_price")
 * @Security("has_role('ROLE_PANEL_TICKET_PRICE')")
 */
class TicketPriceController extends Controller
{
    /**
     * @var CoreSettingService
     * @Inject("model.core.setting")
     */
    private $settingService;


    /**
     * @Route("/index", name="panel_ticket_price")
     * @Template()
     */
    public function indexAction()
    {

        $form = $this->getForm();
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/save", name="panel_ticket_price_save")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {

        $form = $this->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->settingService->set(ConstService::SETTING_TICKET_PRICE, null, $form->getData());
            $this->get('tool.flash')->formSaved();
            return $this->redirectToRoute("panel_ticket_price");
        }

        $this->get('tool.flash')->formError();
        return $this->redirectToRoute("panel_ticket_price");
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function getForm()
    {
        $data = $this->settingService->get(ConstService::SETTING_TICKET_PRICE);
        $form = $this->createForm("setting_ticket_price", $data, [
            'method' => 'POST',
            'action' => $this->generateUrl("panel_ticket_price_save")
        ]);

        return $form;
    }

}
