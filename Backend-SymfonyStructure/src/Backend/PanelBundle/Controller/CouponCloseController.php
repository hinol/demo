<?php

namespace Backend\PanelBundle\Controller;

use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CouponCloseController
 * @package Backend\PanelBundle\Controller
 * @Route("coupon_close")
 * @Security("has_role('ROLE_PANEL_COUPON_CLOSE')")
 */
class CouponCloseController extends Controller
{
    /**
     * @var CoreSettingService
     * @Inject("model.core.setting")
     */
    private $settingService;


    /**
     * @Route("/index", name="panel_coupon_close")
     * @Template()
     */
    public function indexAction()
    {

        $form = $this->getForm();
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @Route("/save", name="panel_coupon_close_save")
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {

        $form = $this->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->settingService->set(ConstService::SETTING_COUPON_CLOSE, null, $form->getData());
            $this->get('tool.flash')->formSaved();
            return $this->redirectToRoute("panel_coupon_close");
        }

        $this->get('tool.flash')->formError();
        return $this->redirectToRoute("panel_coupon_close");
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function getForm()
    {
        $data = $this->settingService->get(ConstService::SETTING_COUPON_CLOSE);
        $form = $this->createForm("setting_coupon_close", $data, [
            'method' => 'POST',
            'action' => $this->generateUrl("panel_coupon_close_save")
        ]);

        return $form;
    }

}
