<?php

namespace Backend\PanelBundle\Controller;

use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GameCalendarController
 * @package Backend\PanelBundle\Controller
 * @Route("/game_calendar")
 * @Security("has_role('ROLE_PANEL_GAME_CALENDAR')")
 */
class GameCalendarController extends Controller
{

    /**
     * @var CoreSettingService
     * @Inject("model.core.setting")
     */
    private $settingService;


    /**
     * @Route("/index", name="panel_game_calendar")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->getForm();
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/save", name="panel_game_calendar_save")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {

        $form = $this->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->settingService->set(ConstService::SETTING_GAME_CALENDAR, null, $form->getData());
            $this->get('tool.flash')->formSaved();
            return $this->redirectToRoute("panel_game_calendar");
        }

        $this->get('tool.flash')->formError();
        return $this->redirectToRoute("panel_game_calendar");
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function getForm()
    {
        $data = $this->settingService->get(ConstService::SETTING_GAME_CALENDAR);
        $form = $this->createForm("setting_calendar_game", $data, [
            'method' => 'POST',
            'action' => $this->generateUrl("panel_game_calendar_save")
        ]);

        return $form;
    }

}
