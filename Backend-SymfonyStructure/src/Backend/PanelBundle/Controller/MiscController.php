<?php

namespace Backend\PanelBundle\Controller;

use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MiscController extends Controller
{


    /**
     * @var CoreSettingService
     * @Inject("model.core.setting")
     */
    private $settingService;

    /**
     * @Route("/index", name="panel_misc")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->getForm();
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/save", name="panel_misc_save")
     * @Template()
     */
    public function saveAction(Request $request)
    {
        $form = $this->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->settingService->set(ConstService::SETTING_MISC, null, $form->getData());
            $this->get('tool.flash')->formSaved();
            return $this->redirectToRoute("panel_misc");
        }

        $this->get('tool.flash')->formError();
        return $this->redirectToRoute("panel_misc");
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function getForm()
    {
        $data = $this->settingService->get(ConstService::SETTING_MISC);
        $form = $this->createForm("setting_misc", $data, [
            'method' => 'POST',
            'action' => $this->generateUrl("panel_misc_save")
        ]);

        return $form;
    }
}
