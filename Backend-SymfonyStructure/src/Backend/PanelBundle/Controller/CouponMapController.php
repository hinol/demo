<?php

namespace Backend\PanelBundle\Controller;

use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use App\ModelBundle\Services\Coupon\CouponService;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CouponMapController
 * @package Backend\PanelBundle\Controller
 * @Route("coupon_map")
 * @Security("has_role('ROLE_PANEL_COUPON_MAP')")
 */
class CouponMapController extends Controller
{
    /**
     * @var CoreSettingService
     * @Inject("model.core.setting")
     */
    private $settingService;


    /**
     * @var CouponService
     * @Inject("model.coupon")
     */
    private $couponService;

    /**
     * @Route("/index", name="panel_coupon_map")
     * @Template()
     */
    public function indexAction()
    {

        $form = $this->getForm();
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/save", name="panel_coupon_map_save")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {

        $form = $this->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->settingService->set(ConstService::SETTING_COUPON_MAP, null, $form->getData());
            $this->get('tool.flash')->formSaved();
            return $this->redirectToRoute("panel_coupon_map");
        }

        $this->get('tool.flash')->formError();
        return $this->redirectToRoute("panel_coupon_map");
    }

    /**
     * @Route("/generate/{system}", name="panel_coupon_map_generate")
     */
    public function generateAction($system)
    {

        $image = $this->couponService->generateCoupon($system, range(1, 46));

        $headers = array(
            'Content-Type' => 'image/png',
            'Content-Disposition' => 'attachment; filename="Coupon test - system' . $system . '.png"'
        );
        return new Response($image, 200, $headers);

    }


    /**
     * @return \Symfony\Component\Form\Form
     */
    private function getForm()
    {
        $data = $this->settingService->get(ConstService::SETTING_COUPON_MAP);
        $form = $this->createForm("setting_coupon_map", $data, [
            'method' => 'POST',
            'action' => $this->generateUrl("panel_coupon_map_save")
        ]);

        return $form;
    }

}