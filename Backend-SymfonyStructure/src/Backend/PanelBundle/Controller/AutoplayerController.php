<?php

namespace Backend\PanelBundle\Controller;

use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AutoplayerController
 * @package Backend\PanelBundle\Controller
 * @Route("auto_player")
 * @Security("has_role('ROLE_PANEL_AUTO_PLAYER')")
 */
class AutoplayerController extends Controller
{
    /**
     * @var CoreSettingService
     * @Inject("model.core.setting")
     */
    private $settingService;


    /**
     * @Route("/index", name="panel_auto_player")
     * @Template()
     */
    public function indexAction()
    {

        $form = $this->getForm();
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     * @Route("/save", name="panel_auto_player_save")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {

        $form = $this->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->settingService->set(ConstService::SETTING_AUTO_PLAYER, null, $form->getData());
            $this->get('tool.flash')->formSaved();
            return $this->redirectToRoute("panel_auto_player");
        }

        $this->get('tool.flash')->formError();
        return $this->redirectToRoute("panel_auto_player");
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function getForm()
    {
        $data = $this->settingService->get(ConstService::SETTING_AUTO_PLAYER);
        $form = $this->createForm("setting_auto_player", $data, [
            'method' => 'POST',
            'action' => $this->generateUrl("panel_auto_player_save")
        ]);

        return $form;
    }

}
