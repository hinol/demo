<?php

namespace Backend\PanelBundle\Controller;

use JMS\DiExtraBundle\Annotation\Inject;
use Lib\Model\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package Backend\PanelBundle\Controller
 */
class SecurityController extends Controller
{
    /**
     * @var CsrfTokenManager
     * @Inject("security.csrf.token_manager")
     */
    private $csrfTokenManager;

    /**
     * @var AuthenticationUtils
     * @Inject("security.authentication_utils")
     */
    private $authentication_utils;


    /**
     * @Route("/login", name="panel_security_login")
     * @Template()
     */
    public function loginAction()
    {
        if ($this->getUser() instanceof User) {
            return $this->redirectToRoute("panel_index");
        }
        return [
            'csrf_token' => $this->csrfTokenManager->getToken("authenticate"),
            'last_username' => $this->authentication_utils->getLastUsername(),
            'error' => $this->authentication_utils->getLastAuthenticationError(),
        ];
    }

    /**
     * @Route("/login_check", name="panel_security_login_check")
     */
    public function loginCheckAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    /**
     * @Route("/logout", name="panel_security_logout")
     */
    public function logoutAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }
}
