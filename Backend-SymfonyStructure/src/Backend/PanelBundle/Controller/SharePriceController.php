<?php

namespace Backend\PanelBundle\Controller;

use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class SharePriceController
 * @package Backend\PanelBundle\Controller
 * @Route("/share_price")
 * @Security("has_role('ROLE_PANEL_SHARE_PRICE')")
 */
class SharePriceController extends Controller
{
    /**
     * @var CoreSettingService
     * @Inject("model.core.setting")
     */
    private $settingService;

    /**
     * @Route("/index", name="panel_share_price")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->getForm();
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/save", name="panel_share_price_save")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {
        $form = $this->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->settingService->set(ConstService::SETTING_SHARE_PRICE, null, $form->getData());
            if($form->getClickedButton() && $form->getClickedButton()->getName() == 'commision_recount'){
                $this->settingService->recountSharePrice();
            }
            $this->get('tool.flash')->formSaved();
            return $this->redirectToRoute("panel_share_price");
        }

        $this->get('tool.flash')->formError();
        return $this->redirectToRoute("panel_share_price");
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function getForm()
    {
        $data = $this->settingService->get(ConstService::SETTING_SHARE_PRICE);
        $form = $this->createForm("setting_share_price", $data, [
            'method' => 'POST',
            'action' => $this->generateUrl("panel_share_price_save")
        ]);

        return $form;
    }

}
