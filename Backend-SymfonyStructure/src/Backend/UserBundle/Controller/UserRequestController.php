<?php

namespace Backend\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class UserRequestController
 * @package Backend\UserBundle\Controller
 * @Route("user_request")
 */
class UserRequestController extends Controller
{
    /**
     * @Route("/index", name="panel_user_request")
     * @Template()
     */
    public function indexAction()
    {
        return array(
                // ...
            );    }

    /**
     * @Route("/download")
     * @Template()
     */
    public function downloadAction()
    {
        return array(
                // ...
            );    }

    /**
     * @Route("/accept")
     * @Template()
     */
    public function acceptAction()
    {
        return array(
                // ...
            );    }

    /**
     * @Route("/reject")
     * @Template()
     */
    public function rejectAction()
    {
        return array(
                // ...
            );    }

}
