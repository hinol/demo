<?php

namespace Backend\UserBundle\Controller;

use App\ModelBundle\Services\Query\User\UserQueryService;
use App\ModelBundle\Services\User\RoleService;
use JMS\DiExtraBundle\Annotation\Inject;
use Lib\Model\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("user_list")
 */
class UserListController extends Controller
{

    /**
     * @var UserQueryService
     * @Inject("query.user")
     */
    private $userQueryService;


    /**
     * @var Request
     * @Inject("request")
     */
    private $request;

    /**
     * @Route("/index/{filter}", name="panel_user")
     * @Template()
     */
    public function indexAction($filter = null)
    {


        return [
            'users' => $this->userQueryService->getQueryByFilter(json_decode($filter))->filterByRole(RoleService::ROLE_PLAYER)->paginate($this->request->get("page")),
            'form' => $this->getFilterForm(json_decode($filter))->createView()

        ];
    }

    /**
     * @Route("/filter", name="panel_user_filter")
     * @Method("POST")
     */
    public function filterAction()
    {
        $form = $this->getFilterForm();
        $form->handleRequest($this->request);
        if ($form->isValid()) {

            return $this->redirectToRoute("panel_user", ['filter' => json_encode($form->getData())]);
        }
    }


    /**
     * @Route("/login/{user}", name="panel_user_login")
     */
    public function loginAction(User $user)
    {
        return $this->redirectToRoute("frontend_login_custom", ['user' => $user->getId(), 'code' => $user->getLoginCode()]);
    }

    private function getFilterForm($filter = null)
    {
        return $this->createForm("backend_user_list_filter", $filter, [
            'action' => $this->generateUrl("panel_user_filter")
        ]);
    }





}
