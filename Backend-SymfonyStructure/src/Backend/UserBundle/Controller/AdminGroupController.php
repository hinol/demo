<?php

namespace Backend\UserBundle\Controller;

use App\ModelBundle\Services\Query\User\UserGroupQueryService;
use App\ToolBundle\Services\FlashService;
use JMS\DiExtraBundle\Annotation\Inject;
use Lib\Model\Group;
use Lib\Model\UserGroup;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminGroupController
 * @package Backend\UserBundle\Controller
 * @Route("/admin_group")
 */
class AdminGroupController extends Controller
{

    /**
     * @var UserGroupQueryService
     * @Inject("query.user.group")
     */
    private $userGroupQueryService;

    /**
     * @var FlashService
     * @Inject("tool.flash")
     */
    private $flash;

    /**
     * @var Request
     * @Inject("request")
     */
    private $request;

    /**
     * @Route("/index", name="panel_admin_group")
     * @Template()
     */
    public function indexAction()
    {
        return ['groups' => $this->userGroupQueryService->paginate($this->request->get("p"))];
    }

    /**
     * @Route("/add", name="panel_admin_group_add")
     * @Template()
     */
    public function addAction()
    {
        return ['form' => $this->getForm()->createView()];
    }

    /**
     * @Route("/edit/{group}", name="panel_admin_group_edit")
     * @ParamConverter("param_group")
     * @Template()
     * @param Group $group
     * @return array
     */
    public function editAction(Group $group)
    {
        return ['form' => $this->getForm($group)->createView(), 'group'=>$group];
    }

    /**
     * @Route("/delete/{group}", name="panel_admin_group_delete")
     * @Template()
     * @param Group $group
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Group $group)
    {
        if ($this->request->getMethod() == Request::METHOD_POST) {
            $group->delete();
            $this->flash->removed();
            return $this->redirectToRoute("panel_admin_group");
        }
        return [];
    }

    /**
     * @Route("/save/{group}", name="panel_admin_group_save")
     * @ParamConverter("param_group")
     * @Method("POST")
     * @param Group $group
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function saveAction(Group $group = null)
    {
        $form = $this->getForm($group);
        $form->handleRequest($this->request);
        if ($form->isValid()) {
            $group->save();
            $this->flash->formSaved();
            return $this->redirectToRoute("panel_admin_group_edit", ['group' => $group->getId()]);
        }
        $this->flash->formError();
        return $this->render("@BackendUser/AdminGroup/edit.html.twig", ['form' => $form->createView()]);
    }


    /**
     * @param Group|null $model
     * @return \Symfony\Component\Form\Form
     */
    public function getForm(Group $model = null)
    {
        $action = $this->generateUrl("panel_admin_group_save");
        if ($model) {
            $action = $this->generateUrl("panel_admin_group_save", ['group' => $model->getId()]);
        }

        $form = $this->createForm("panel_user_group", $model, [
            'method' => 'POST',
            'action' => $action
        ]);
        return $form;
    }
}
