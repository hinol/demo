<?php

namespace Backend\UserBundle\Controller;

use App\ToolBundle\Services\FlashService;
use JMS\DiExtraBundle\Annotation\Inject;
use Lib\Model\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserDetailController
 * @package Backend\UserBundle\Controller
 * @Route("user_detail")
 */
class UserDetailController extends Controller
{


    /**
     * @var Request
     * @Inject("request")
     */
    private $request;


    /**
     * @var FlashService
     * @Inject("tool.flash")
     */
    private $flash;

    /**
     * @Route("/{user}/index", name="panel_user_detail")
     * @ParamConverter("param_user")
     * @Template()
     */
    public function indexAction(User $user)
    {


        $form = $this->createForm("backend_user_edit", $user);

        $form->handleRequest($this->request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $user = $form->getData();
                $user->save();
                $this->flash->formSaved();
                return $this->redirectToRoute("panel_user_detail", ['user' => $user->getId()]);
            } else {
                $this->flash->formInvalid();
            }
        }

        return ['user' => $user, 'form' => $form->createView()];

    }

    /**
     * @Route("/{user}/edit", name="panel_user_detail_edit")
     * @ParamConverter("param_user")
     * @Template()
     */
    public
    function editAction(User $user)
    {
        return ['user' => $user];

    }

    /**
     * @Route("/{user}/history", name="panel_user_detail_history")
     * @ParamConverter("param_user")
     * @Template()
     */
    public
    function historyAction(User $user)
    {
        return ['user' => $user];

    }

    /**
     * @Route("/{user}/wins", name="panel_user_detail_wins")
     * @ParamConverter("param_user")
     * @Template()
     */
    public
    function winsAction(User $user)
    {
        return ['user' => $user];

    }

    /**
     * @Route("/{user}/amount", name="panel_user_detail_amount")
     * @ParamConverter("param_user")
     * @Template()
     */
    public
    function amountAction(User $user)
    {
        return ['user' => $user];

    }

    /**
     * @Route("/{user}/note", name="panel_user_detail_note")
     * @ParamConverter("param_user")
     * @Template()
     */
    public
    function noteAction(User $user)
    {
        return ['user' => $user];

    }

    /**
     * @Route("/{user}/save", name="panel_user_detail_save")
     * @ParamConverter("param_user")
     * @Template()
     */
    public
    function saveAction(User $user)
    {
        return ['user' => $user];

    }

    /**
     * @Route("/{user}/delete", name="panel_user_detail_delete")
     * @ParamConverter("param_user")
     * @Template()
     */
    public
    function deleteAction(User $user)
    {
        return ['user' => $user];

    }

}
