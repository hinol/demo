<?php

namespace Backend\UserBundle\Controller;

use App\ModelBundle\Services\Query\User\UserQueryService;
use App\ModelBundle\Services\User\RoleService;
use App\ToolBundle\Services\FlashService;
use JMS\DiExtraBundle\Annotation\Inject;
use Lib\Model\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminListController
 * @package Backend\UserBundle\Controller
 * @Route("admin_list")
 */
class AdminListController extends Controller
{

    /**
     * @var UserQueryService
     * @Inject("query.user")
     */
    private $userQueryService;

    /**
     * @var FlashService
     * @Inject("tool.flash")
     */
    private $flash;

    /**
     * @var Request
     * @Inject("request")
     */
    private $request;

    /**
     * @Route("/index", name="panel_admin_list")
     * @Template()
     */
    public function indexAction()
    {
        return ['users' => $this->userQueryService->getQuery()->filterByRole(RoleService::ROLE_PANEL)->paginate($this->request->get("page"))];
    }

    /**
     * @Route("/add", name="panel_admin_list_add")
     * @Template()
     */
    public function addAction()
    {
        return ['form' => $this->getForm()->createView()];
    }

    /**
     * @Route("/edit/{user}", name="panel_admin_list_edit")
     * @ParamConverter("param_user")
     * @Template()
     * @param User $user
     * @return array
     */
    public function editAction(User $user)
    {
        return ['form' => $this->getForm($user)->createView(), 'user' => $user];

    }

    /**
     * @Route("/status/{user}", name="panel_admin_list_user")
     * @ParamConverter("param_user")
     * @Template()
     * @param User $user
     * @return array
     */
    public function statusAction(User $user = null)
    {
        return $user;
    }

    /**
     * @Route("/delete/{user}", name="panel_admin_list_delete")
     * @ParamConverter("param_user")
     * @Template()
     * @param User $user
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(User $user)
    {
        if ($this->request->getMethod() == Request::METHOD_POST) {
            $user->delete();
            $this->flash->removed();
            return $this->redirectToRoute("panel_admin_list");
        }
        return ['user' => $user];
    }

    /**
     * @Route("/save/{user}", name="panel_admin_list_save")
     * @ParamConverter("param_user")
     * @Method("POST")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function saveAction(User $user = null)
    {
        if (null == $user) {
            $user = new User();
        }
        $form = $this->getForm($user);
        $form->handleRequest($this->request);
        if ($form->isValid()) {
            $user->addRole(RoleService::ROLE_PANEL);
            $user->save();
            $this->flash->formSaved();
            return $this->redirectToRoute("panel_admin_list_edit", ['user' => $user->getId()]);
        }
        $this->flash->formError();
        return $this->render("BackendUserBundle:AdminList:edit.html.twig", ['form' => $form->createView()]);
    }

    /**
     * @param User|null $model
     * @return \Symfony\Component\Form\Form
     */
    public function getForm(User $model = null)
    {
        $action = $this->generateUrl("panel_admin_list_save");
        if ($model) {
            $action = $this->generateUrl("panel_admin_list_save", ['user' => $model->getId()]);
        }

        $form = $this->createForm("panel_user_admin", $model, [
            'method' => 'POST',
            'action' => $action
        ]);
        return $form;
    }
}
