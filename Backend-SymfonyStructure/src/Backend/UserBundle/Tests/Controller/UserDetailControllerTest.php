<?php

namespace Backend\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserDetailControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testEdit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/edit');
    }

    public function testHistory()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/history');
    }

    public function testWins()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/wins');
    }

    public function testAmount()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/amount');
    }

    public function testNote()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/note');
    }

    public function testSave()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/save');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/delete');
    }

}
