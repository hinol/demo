<?php

namespace Backend\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRequestControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testDownload()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/download');
    }

    public function testAccept()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/accept');
    }

    public function testReject()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/reject');
    }

}
