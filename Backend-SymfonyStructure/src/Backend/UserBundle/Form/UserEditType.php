<?php

namespace Backend\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("firstname")
            ->add("lastname")
            ->add("email")
            ->add("phone")
            ->add("enabled")
            ->add("user_data", 'backend_user_data_edit')
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lib\Model\User',
            'validation_groups' => array('user'),

        ));
    }

    public function getName()
    {
        return 'backend_user_edit';
    }
}
