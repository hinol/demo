<?php


namespace Backend\UserBundle\Form;

use App\ModelBundle\Services\User\RoleService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class GroupType extends AbstractType
{

    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add("id", "hidden")
            ->add("name", "text")
            ->add("roles", 'choice', [
                'multiple' => true,
                'expanded' => true,
                'choices' => RoleService::getPanelRolesAsAssocArray()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lib\Model\Group',
        ));
    }


    /** @inheritdoc */
    public function getName()
    {
        return "panel_user_group";
    }
}