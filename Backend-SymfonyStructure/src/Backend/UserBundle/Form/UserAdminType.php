<?php


namespace Backend\UserBundle\Form;

use App\ModelBundle\Services\Query\User\UserGroupQueryService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class UserAdminType
 * @package Backend\UserBundle\Form
 */
class UserAdminType extends AbstractType
{

    /**
     * @var UserGroupQueryService
     */
    private $userGroupQueryService;

    /**
     * @param UserGroupQueryService $userGroupQueryService
     */
    public function __construct(UserGroupQueryService $userGroupQueryService)
    {
        $this->userGroupQueryService = $userGroupQueryService;
    }


    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add("id", "hidden")
            ->add("email", "email")
            ->add("firstname", "text")
            ->add("lastname", "text")
            ->add("enabled", "checkbox")
            ->add("plain_password", "repeated",
                [
                    'required' => false,
                    'type' => 'password',
                ])
            ->add("groups", "model", [
                "class" => 'Lib\Model\Group',
                "property" => "name",
                "multiple" => true,
                "expanded" => true,
            ]);


    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lib\Model\User',
            'validation_groups' => array('user'),
        ));
    }


    /** @inheritdoc */
    public function getName()
    {
        return "panel_user_admin";
    }


}