<?php

namespace Backend\TestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IndexControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testRun()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/run');
    }

}
