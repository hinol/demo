<?php

namespace Backend\TestBundle\Controller;

use App\ModelBundle\Services\Bet\BetGameService;
use App\ModelBundle\Services\Bet\BetService;
use App\ModelBundle\Services\Bet\BetShareService;
use App\ModelBundle\Services\Query\Bet\BetGameQueryService;
use App\ModelBundle\Services\Query\Bet\BetQueryService;
use App\ModelBundle\Services\Query\Bet\BetShareQueryService;
use App\ModelBundle\Services\Query\Finance\TransactionQueryService;
use App\ModelBundle\Services\Query\User\UserDataQueryService;
use App\ModelBundle\Services\Query\User\UserQueryService;
use App\ModelBundle\Services\User\RoleService;
use App\ModelBundle\Services\User\UserService;
use App\ToolBundle\Services\BackupService;
use App\ToolBundle\Services\FlashService;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{

    /**
     * @var Request
     * @Inject("request")
     */
    private $request;

    /**
     * @var FlashService
     * @Inject("tool.flash")
     */
    private $flash;

    /**
     * @var UserService
     * @Inject("model.user")
     */
    private $userService;


    /**
     * @var BetService
     * @Inject("model.bet")
     */
    private $betService;


    /**
     * @var BetShareService
     * @Inject("model.bet.share")
     */
    private $betShareService;

    /**
     * @Route("/index", name="panel_test")
     * @Template()
     */
    public function indexAction()
    {
        $result = [];
        $forms = [
            'user' => 'backend_test_user_generate',
            'bet' => 'backend_test_bet_generate',
            'betShare' => 'backend_test_bet_share_generate',
            'game' => 'backend_test_game_generate',
            'paycheck' => 'backend_test_paycheck_generate',
            'clear' => 'backend_test_clear_generate',
            'backup' => 'backend_test_backup_generate'
        ];
        foreach ($forms as $k => $formName) {
            $form = $this->createForm($formName);
            $form->handleRequest($this->request);
            if ($form->isSubmitted() && $form->isValid()) {
                $method = "generate" . ucfirst($k);
                $this->$method($form->getData());
                $this->flash->formSaved();
                return $this->redirectToRoute("panel_test");
            }
            $result['forms'][$k] = $form->createView();

        }


        $result['backup_list'] = $this->backupService->getList();

        return $result;
    }


    /**
     * @var UserQueryService
     * @Inject("query.user")
     */
    private $userQueryService;

    /**
     * @var UserDataQueryService
     * @Inject("query.user.data")
     */
    private $userDataQueryService;

    /**
     * @var BetGameQueryService
     * @Inject("query.bet.game")
     */
    private $gameQueryService;


    /**
     * @var BetQueryService
     * @Inject("query.bet")
     */
    private $betQueryService;


    /**
     * @var BetShareQueryService
     * @Inject("query.bet.share")
     */
    private $betShareQueryService;

    /**
     * @var TransactionQueryService
     * @Inject("query.finance.transaction")
     */
    private $transactionQueryService;


    /**
     * @var BetGameService
     * @Inject("model.bet.game")
     */
    private $gameService;


    /**
     * @var BackupService
     * @Inject("tool.backup")
     */
    private $backupService;

    /**
     * @param $data
     * @throws \Propel\Runtime\Exception\PropelException
     */

    private function generateClear($data)
    {

        foreach ($data as $name => $value) {
            if ($value !== true) {
                continue;
            }
            switch ($name) {

                case "players":
                    $this->userQueryService->getQuery()->filterByRole(RoleService::ROLE_PLAYER)->delete();
                    break;
                case "games":
                    $this->gameQueryService->getQuery()->deleteAll();
                    break;
                case "bets":
                    $this->betQueryService->getQuery()->deleteAll();
                    break;
                case "betShares":
                    $this->betShareQueryService->getQuery()->deleteAll();

                    break;
                case "transactions":
                    $this->transactionQueryService->getQuery()->deleteAll();
                    break;
            }

        }


    }


    private function generateUser($data)
    {
        for ($i = 0; $i < $data['cnt']; $i++) {
            $this->userService->generatePlayer();
        }
    }


    private function generateBet($data)
    {
        for ($i = 0; $i < $data['cnt']; $i++) {
            $this->betService->generateBet($data['game_type'], $data['system']);
        }
    }

    private function generateBetShare($data)
    {
        for ($i = 0; $i < $data['cnt']; $i++) {
            try {
                $this->betShareService->generateBetShare($data['min'], $data['max']);
            } catch (\Exception $e) {
                //silence
            }

        }
    }

    private function generateGame($data)
    {

        switch ($data['action']) {
            case "check":
                $this->gameService->generateGames();
                break;
            case "generate":

                $this->gameService->recountAssigned();
                break;
        }
    }

    public function generateBackup($data)
    {
        $this->backupService->make($data['name']);

    }

    /**
     * @param $filePath
     * @Route("/restoreBackup", name="panel_test_restore")
     */
    public function restoreBackupAction()
    {

        $filePath = $this->request->get("filePath");
        $this->backupService->restore($filePath);
        $this->flash->addSuccess("Backup restored");
        return $this->redirectToRoute("panel_test");
    }

}
