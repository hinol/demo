<?php

namespace Backend\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClearGenerateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("players", "checkbox", ['required' => false])
            ->add("games", "checkbox", ['required' => false])
            ->add("bets", "checkbox", ['label' => "Bet (clear also bet shares)", 'required' => false])
            ->add("betShares", "checkbox", [ 'required' => false])
            ->add("transactions", "checkbox", ['required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'backend_test_clear_generate';
    }
}
