<?php

namespace Backend\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameGenerateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("action", "choice", [
                'choices' => [
                    "check" => "Check new games",
                    "generate" => "Generate games wins (make sure that game are in status assigned)"
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'backend_test_game_generate';
    }
}
