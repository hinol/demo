<?php

namespace Backend\TestBundle\Form;

use App\ModelBundle\Services\ConstService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BetGenerateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("game_type", "choice", [
                'choices' => ConstService::getGamesAsAssocArray()
            ])
            ->add("cnt")
            ->add("system", "choice", [
                'choices' => ConstService::getSystemsAsAssocArray()
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'backend_test_bet_generate';
    }
}
