<?php

namespace Backend\TestBundle\Form;

use App\ModelBundle\Services\Core\CoreSettingService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BetShareGenerateType
 * @package Backend\TestBundle\Form
 */
class BetShareGenerateType extends AbstractType
{

    /**
     * @var CoreSettingService
     */
    private $coreSettingService;

    /**
     * @param CoreSettingService $coreSettingService
     */
    public function __construct(CoreSettingService $coreSettingService)
    {
        $this->coreSettingService = $coreSettingService;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("min", 'integer', [
                'attr' => [
                    'min' => 0,
                    'max' => $this->coreSettingService->getMaxShare()
                ]
            ])
            ->add("max", 'integer', [
                'attr' => [
                    'min' => 0,
                    'max' => $this->coreSettingService->getMaxShare()
                ]
            ])
            ->add("cnt");
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'backend_test_bet_share_generate';
    }
}
