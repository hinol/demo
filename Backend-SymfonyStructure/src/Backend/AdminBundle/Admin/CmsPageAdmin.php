<?php

namespace Backend\AdminBundle\Admin;

use Lib\Model\CmsPage;
use Lib\Model\CmsPagePeer;
use Lib\Model\CmsPageQuery;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class CmsPageAdmin extends Admin
{

    protected $baseRouteName = 'admin_cms_page';
    protected $baseRoutePattern = 'admin_cms_page';

    protected function configureFormFields(FormMapper $formMapper)
    {


        $formMapper
            ->with("Main Data", ['class' => 'col-md-4'])
            ->add('title')
            ->add("body_h1")
            ->add("description")
            ->add('url')
            ->end()
            ->with("Tools", ['class' => 'col-md-4'])
            ->add('parent', 'model', array(
                'class' => 'Lib\Model\CmsPage',
                'required' => false,
            ))
            ->add('name')
            ->add("type")
            ->end()
            ->with("Seo", ['class' => 'col-md-4'])
            ->add('seo_title')
            ->add('seo_description')
            ->add('seo_robots')
            ->end();


        $formMapper->add('format_type', 'sonata_formatter_type', array(
            'source_field' => 'body_raw',
            'source_field_options' => array('attr' => array('class' => 'span10', 'rows' => 20)),
            'format_field' => 'body_format_type',
            'target_field' => 'body',
            'ckeditor_context' => 'default',
            'event_dispatcher' => $formMapper->getFormBuilder()->getEventDispatcher()
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier("id")
            ->addIdentifier("title")
            ->add("description")
            ->add("type")
            ->add("parent")
            ->add("version")
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    public function preUpdate($object)
    {

        if (false == ($object instanceof CmsPage)) {
            return true;
        }

        if ($object->getParent()) {

            if ($object->getParent() instanceof CmsPage) {
                $parent = $object->getParent();
            } else {
                $parent = CmsPageQuery::create()->findOneById($object->getParent());
            }

            if ($object->hasParent()) {
                $object->moveToLastChildOf($parent);
            } else {
                $object->insertAsLastChildOf($parent);
            }
        }
    }
}