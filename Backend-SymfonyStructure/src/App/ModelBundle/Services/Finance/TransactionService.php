<?php


namespace App\ModelBundle\Services\Finance;


use App\ModelBundle\Services\Query\Finance\TransactionQueryService;
use Lib\Model\BetShare;
use Lib\Model\FinanceTransaction;
use Lib\Model\Map\FinanceTransactionTableMap;
use Lib\Model\User;

/**
 * Class TransactionService
 * @package App\ModelBundle\Services\Finance
 */
class TransactionService
{

    /**
     * @var TransactionQueryService
     */
    private $transactionQueryService;

    /**
     * @param TransactionQueryService $transactionQueryService
     */
    public function __construct(TransactionQueryService $transactionQueryService)
    {
        $this->transactionQueryService = $transactionQueryService;
    }

    /**
     * @param $amount
     * @param User $user
     * @return $this|FinanceTransaction
     * @throws \Propel\Runtime\Exception\PropelException
     */
    private function newTransaction($amount, User $user)
    {
        $t = (new FinanceTransaction())
            ->setUser($user)
            ->setAmount($amount)
            ->setStatus(FinanceTransactionTableMap::COL_STATUS_NEW);
        $t->save();
        $t->setStatus(FinanceTransactionTableMap::COL_STATUS_SUCCESS);


        $userAmount = $user->getUserData()->getAmount() + $amount;
        $t->setUserAmount($userAmount);

        $t->save();


        $user->getUserData()->setAmount($userAmount);
        $user->getUserData()->save();


        return $t;
    }

    /**
     * @param BetShare $betShare
     * @param null $price
     * @return $this|TransactionService|FinanceTransaction
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function newTransactionFromBetShare(BetShare $betShare, $price = null)
    {
        if (null === $price) {

            $price = -1 * $betShare->getPrice();
        }

        $transaction = $this->newTransaction($price, $betShare->getUser());
        $transaction->setOperationtype(FinanceTransactionTableMap::COL_OPERATION_TYPE_SHARES);
        $transaction->save();
        return $transaction;
    }

    /**
     * @param User $user
     * @param $amount
     * @return bool
     */
    public
    function isValidTransaction(User $user, $amount)
    {
        if ($user->getUserData()->getAmount() > $amount) {
            return true;
        }
        return false;
    }

    /**
     * @param $amount
     * @param User $user
     * @return $this|TransactionService|FinanceTransaction
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function newPayment($amount, User $user)
    {

        $transaction = $this->newTransaction($amount, $user);
        $transaction->setOperationType(FinanceTransactionTableMap::COL_OPERATION_TYPE_PAYMENT);
        $transaction->save();
        return $transaction;
    }
}