<?php


namespace App\ModelBundle\Services\Core;


use App\ModelBundle\Services\Query\Core\CoreMailQueryService;

/**
 * Class CoreMailService
 * @package App\ModelBundle\Services\Core
 */
class CoreMailService
{

    /**
     * @var CoreMailQueryService
     */
    private $coreMailQueryService;

    /**
     * @param CoreMailQueryService $coreMailQueryService
     */
    public function __construct(CoreMailQueryService $coreMailQueryService)
    {
        $this->coreMailQueryService = $coreMailQueryService;
    }
}