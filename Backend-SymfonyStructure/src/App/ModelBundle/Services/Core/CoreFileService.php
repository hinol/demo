<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 11/05/15
 * Time: 11:53
 */

namespace App\ModelBundle\Services\Core;


use App\ModelBundle\Services\Query\Core\CoreFileQueryService;
use Buzz\Browser;
use Lib\Model\CoreFile;
use Lib\Model\CoreFilePeer;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Class FileService
 * @package App\ModelBundle\Services
 */
class CoreFileService
{

    /**
     * @var CoreFileQueryService
     */
    private $fileQueryService;


    /**
     * @var Browser
     */
    private $browser;




    /**
     * @var string
     */
    private $fileDataPath;


    const  ROUTE = "core_file";

    /**
     * @var Router
     */
    private $router;

    /**
     * @var string
     */
    private $rootPath;

    /**
     * @var array
     */
    private $fileSize;

    /**
     * @param CoreFileQueryService $fileQueryService
     * @param Browser $browser
     * @param $fileDataPath
     * @param Router $router
     * @param $rootPath
     * @param $fileSize
     */
    public function __construct(CoreFileQueryService $fileQueryService, Browser $browser, $fileDataPath, Router $router, $rootPath, $fileSize)
    {
        $this->fileQueryService = $fileQueryService;
        $this->browser = $browser;
        $this->fileDataPath = $fileDataPath;
        $this->router = $router;
        $this->rootPath = $rootPath;
        $this->fileSize = $fileSize;
    }


    /**
     * @param $url
     * @return CoreFile
     */
    public function createFromUrl($url)
    {

        $file = $this->fileQueryService->getQuery()->findOneBySource($url);
        if ($file instanceof CoreFile) {
            return $file;
        }

        return $this->createFromBlob($this->browser->get($url)->getContent(), $url);
    }


    /**
     * @param CoreFile $file
     * @param $size
     * @param Request $request
     * @return \Imagick
     * @throws \Exception
     */
    public function getImage(CoreFile $file, $size, Request $request = null)
    {

        if ($file->isImage() === false) {
            throw new \Exception("File is not an image");
        }

        $im = new \Imagick($this->fileDataPath . $file->getFilePath());

        $sizes = $this->fileSize['size'][$size];

        $width = $sizes['width'];
        $height = $sizes['height'];

        $fill = true;
        if (isset($sizes['fill'])) {
            $fill = $sizes['fill'];
        }

        $im->thumbnailImage($width, $height, true, $fill);


        if ($request) {
            $fileToSave = $this->rootPath . $request->getPathInfo();
            $dir = dirname($fileToSave);
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            $im->writeImage($fileToSave);
        }

        return $im;
    }

    /**
     * @param $blob
     * @param null $url
     * @return CoreFile
     * @throws \Exception
     * @throws \PropelException
     */
    public function createFromBlob($blob, $url = null)
    {
        $fileName = md5(uniqid());
        $filePath = substr($fileName, 0, 2) . DIRECTORY_SEPARATOR . substr($fileName, 2, 2) . DIRECTORY_SEPARATOR . $fileName;
        $finalDestination = $this->fileDataPath . DIRECTORY_SEPARATOR . $filePath;
        $dir = dirname($finalDestination);

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        file_put_contents($finalDestination, $blob);

        $file = new CoreFile();
        if ($url) {
            $file->setSource($url);
            $file->setName(basename($url));
        } else {
            $file->setSource("blob");
            $file->setName(uniqid());
        }

        $file->setFileName($fileName);
        $file->setFilePath($filePath);
        $file->setFilesize(strlen($blob));

        $sfFile = new File($finalDestination);
        $mime = $sfFile->getMimeType();

        $file->setMimetype($mime);

        $firstMime = current(explode("/", $mime));

        if ($firstMime === 'image') {

            $im = new \Imagick();
            $im->readImageBlob($blob);

            $file->setWidth($im->getImageWidth());
            $file->setHeight($im->getImageHeight());
            $file->setType(CoreFilePeer::TYPE_IMAGE);

        } else {
            throw new \Exception("Unknow mime " . $mime);
        }


        $file->save();
        return $file;
    }

    /**
     * @param CoreFile $file
     * @param $size
     * @return string
     */
    public function getUrl(CoreFile $file, $size)
    {
        return $this->router->generate(self::ROUTE, ['file' => $file->getId(), 'size' => $size]);
    }


}