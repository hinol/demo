<?php


namespace App\ModelBundle\Services\Core;


use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Query\Core\CoreSettingQueryService;
use App\ToolBundle\Services\ArrayTransformService;

/**
 * Class CoreSettingService
 * @package App\ModelBundle\Services\Core
 */
class CoreSettingService
{


    /**
     * @var CoreSettingQueryService
     */
    private $coreSettingQueryService;

    /**
     * @var ArrayTransformService
     */
    private $arrayTransformService;

    /**
     * @var array
     */
    private $cache;

    /**
     * @param CoreSettingQueryService $coreSettingQueryService
     * @param ArrayTransformService $arrayTransformService
     */
    public function __construct(CoreSettingQueryService $coreSettingQueryService, ArrayTransformService $arrayTransformService)
    {
        $this->coreSettingQueryService = $coreSettingQueryService;
        $this->arrayTransformService = $arrayTransformService;
    }

    /**
     * @param $group
     * @param null $name
     * @return array
     */
    public function get($group, $name = null)
    {

        $cacheKey = $group . $name;

        if (isset($this->cache[$cacheKey])) {
            return $this->cache[$cacheKey];
        }

        $query = $this->coreSettingQueryService->getQuery()
            ->filterByGroup($group);

        if ($name !== null) {
            $query->filterByName($name);
        }
        $result = $query->find();


        if ($result->count() == 1) {
            return $result->getCurrent()->getValue();
        }

        $resultArray = [];
        foreach ($result as $row) {
            $keys = explode("/", $row->getName());
            switch (count($keys)) {
                case 2:
                    $resultArray[(string)$keys[0]][(string)$keys[1]] = $row->getValue();
                    break;
                case 1:
                    $resultArray[(string)$keys[0]] = $row->getValue();
                    break;
            }

        }

        $this->cache[$cacheKey] = $resultArray;
        return $resultArray;
    }

    /**
     * @return array
     */
    public function getHoursRange()
    {
        $date = date("Y-m-d");
        $start = new \DateTime($date);
        $end = (new \DateTime($date))->modify("+1 day");

        $date = $start;
        $hours = [];
        while ($date < $end) {
            $hours[$date->format("H:i")] = $date->format("H:i");
            $date->modify("+10 min");
        }
        return $hours;

    }

    /**
     * @param $group
     * @param null $name
     * @param null $value
     * @return array|\Lib\Model\CoreSetting
     * @throws \Exception
     */
    public function set($group, $name = null, $value = null)
    {

        $cacheKey = $group . $name;

        if (isset($this->cache[$cacheKey])) {
            unset($this->cache[$cacheKey]);
        }

        if (is_string($value)) {
            return $this->setString($group, $name, $value);
        }

        if (is_array($value)) {
            return $this->setArray($group, $name, $value);
        }

        throw new \Exception("Bad Parameter Value");
    }

    /**
     * @param $group
     * @param null $name
     * @param null $value
     * @return \Lib\Model\CoreSetting
     * @throws \Exception
     * @throws \PropelException
     */
    private function setString($group, $name = null, $value = null)
    {

        $setting = $this->coreSettingQueryService->getQuery()
            ->filterByGroup($group)
            ->filterByName($name)
            ->findOneOrCreate();
        $setting->setValue($value);
        $setting->save();
        return $setting;
    }


    /**
     * @param $group
     * @param null $name
     * @param null $value
     * @return array
     */
    private function setArray($group, $name = null, $value = null)
    {
        $values = $this->arrayTransformService->makeFlat($value, '/');
        foreach ($values as $key => $value) {
            $this->setString($group, $key, $value);
        }
        return $this->get($group);


    }


    /**
     * @return array
     */
    public function getShareRange()
    {
        $result = [];
        $v1 = range(0, 100);
        foreach ($v1 as $val) {
            $val = $val / 10;

            $result[(string)$val] = (string)$val;
        }
        $v1 = range(10, 100, 1);
        foreach ($v1 as $val) {
            $result[(string)$val] = (string)$val;
        }
        return $result;

    }

    /**
     * @return mixed
     */
    public function getMaxShare()
    {
        return $this->get("share_price")['share_count'];
    }

    /**
     * @throws \Exception
     */
    public function recountSharePrice()
    {

        $ticketPrice = $this->get("ticket_price");


        $sharePrice = $this->get("share_price");

        $shareCnt = $sharePrice['share_count'];


        foreach (range(7, 12) as $i) {
            $lotto = ($ticketPrice[ConstService::LOTTO][$i] * (1 + $sharePrice['commision'] / 100)) / $shareCnt;
            $ml = ($ticketPrice[ConstService::MINI_LOTTO][$i] * (1 + $sharePrice['commision'] / 100)) / $shareCnt;

            if ($sharePrice['commision_button_ceil'] === true) {
                $lotto = ceil($lotto);
                $ml = ceil($ml);
            }
            $lotto = round($lotto, 2);
            $ml = round($ml, 2);


            $sharePrice[ConstService::LOTTO][$i] = $lotto;
            $sharePrice[ConstService::MINI_LOTTO][$i] = $ml;
        }

        var_dump($sharePrice);

        $this->set("share_price", null, $sharePrice);


        return $this->get("share_price");


    }

    public function getMainWin($type)
    {
        return $this->get(ConstService::SETTING_MISC)['main_win_' . $type];
    }


}