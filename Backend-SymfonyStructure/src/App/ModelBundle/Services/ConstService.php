<?php

namespace App\ModelBundle\Services;


/**
 * Class ConstService
 * @package App\ModelBundle\Services
 */
class ConstService
{

    /**
     *
     */
    const GAME = "game";

    /**
     *
     */
    const LOTTO = "l";
    /**
     *
     */
    const LOTTO_FULL = "lotto";
    /**
     *
     */
    const MINI_LOTTO = "ml";
    /**
     *
     */
    const MINI_LOTTO_FULL = "minilotto";


    /**
     *
     */
    const SETTING_GAME = "game";
    /**
     *
     */
    const SETTING_GAME_CALENDAR = "game_calendar";
    /**
     *
     */
    const SETTING_CALENDAR = "calendar";
    /**
     *
     */
    const SETTING_PRICE = "price";


    /**
     *
     */
    const SETTING_TICKET_PRICE = "ticket_price";


    /**
     *
     */
    const MESSAGE_FORM_SAVED = "form_saved";
    /**
     *
     */
    const MESSAGE_FORM_ERROR = "form_error";


    /**
     *
     */
    const SETTING_SHARE_PRICE = "share_price";

    /**
     *
     */
    const SETTING_COUPON_CLOSE = "coupon_close";
    /**
     *
     */
    const SETTING_COUPON_MAP = "coupon_map";
    /**
     *
     */
    const SETTING_AUTO_PLAYER = "auto_player";


    /**
     *
     */
    const MESSAGE_MODEL_REMOVED = "model_removed";
    /**
     *
     */
    const MESSAGE_LOGIN_SUCCESS = "login_success";
    /**
     *
     */
    const MESSAGE_REGISTER_SUCCESS = "register_success";
    /**
     *
     */
    const MESSAGE_SYSTEM_FAILURE = "message_system_failure";

    /**
     *
     */
    const SMS_CONFIRM_CODE = "sms_confirm_code";
    /**
     *
     */
    const MESSAGE_SMS_SUCCESS = "message_sms_success";
    /**
     *
     */
    const MESSAGE_SMS_FAILURE = "message_sms_failure";
    /**
     *
     */
    const MESSAGE_FORM_INVALID = "message_form_invalid";
    /**
     *
     */
    const MESSAGE_FORM_OLD_PASSWORD_INVALID = "message_form_old_password_invalid";


    const MAX_NUMBER = 43;
    const DEFAULT_GAME_TYPE = self::LOTTO;
    const DEFAULT_SYSTEM = 7;
    const SETTING_MISC = "misc";

    /**
     * @return array
     */
    public static function getSystems()
    {
        return [7, 8, 9, 10, 11, 12];
    }

    /**
     * @return array
     */
    public static function getGamesAsAssocArray()
    {
        return [
            self::LOTTO => self::LOTTO_FULL,
            self::MINI_LOTTO => self::MINI_LOTTO_FULL,
        ];
    }

    /**
     * @return array
     */
    public static function getSystemsAsAssocArray()
    {
        return [7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12];
    }

}