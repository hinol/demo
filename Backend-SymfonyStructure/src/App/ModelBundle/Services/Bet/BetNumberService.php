<?php


namespace App\ModelBundle\Services\Bet;


use App\ModelBundle\Services\Query\Bet\BetNumberQueryService;

/**
 * Class BetNumberService
 * @package App\ModelBundle\Services\Bet
 */
class BetNumberService
{

    /**
     * @var BetNumberQueryService
     */
    private $betNumberQueryService;

    /**
     * @param BetNumberQueryService $betNumberQueryService
     */
    public function __construct(BetNumberQueryService $betNumberQueryService)
    {
        $this->betNumberQueryService = $betNumberQueryService;
    }
}