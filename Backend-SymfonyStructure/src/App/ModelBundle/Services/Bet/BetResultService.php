<?php


namespace App\ModelBundle\Services\Bet;


use App\ModelBundle\Services\Query\Bet\BetResultQueryService;
use Lib\Model\Bet;
use Lib\Model\BetResult;
use Lib\Model\Map\BetNumberTableMap;

/**
 * Class BetResultService
 * @package App\ModelBundle\Services\Bet
 */
class BetResultService
{


    /**
     * @var BetResultQueryService
     */
    private $betResultQueryService;

    /**
     * @param BetResultQueryService $betResultQueryService
     */
    public function __construct(BetResultQueryService $betResultQueryService)
    {
        $this->betResultQueryService = $betResultQueryService;
    }

    /**
     * @param Bet $bet
     * @return BetResult
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function createByBet(Bet $bet)
    {
        $game = $bet->getGame();

        $betResult = new BetResult();
        $betResult->setBet($bet);

        $betNumbers = $bet->getNumbersAsArray(BetNumberTableMap::COL_MODE_BET);
        $winNumbers = $bet->getNumbersAsArray(BetNumberTableMap::COL_MODE_RESULT);


        $win3 = $this->getWinCnt(3, $betNumbers, $winNumbers);
        $win4 = $this->getWinCnt(4, $betNumbers, $winNumbers);
        $win5 = $this->getWinCnt(5, $betNumbers, $winNumbers);
        $win6 = $this->getWinCnt(6, $betNumbers, $winNumbers);

        $betResult
            ->setWin3cnt(count($win3))->setWin3($game->getWin3())
            ->setWin4cnt(count($win4))->setWin4($game->getWin4())
            ->setWin5cnt(count($win5))->setWin5($game->getWin5())
            ->setWin6cnt(count($win6))->setWin6($game->getWin6())
            ->recountAllWin()
        ;

        $betResult->save();
        return $betResult;

    }

    public function getWinCnt($cnt, $betNumbersIn, $winNumbersIn)
    {

        $this->getNumbers($betNumbersIn, $cnt, $betNumbers);
        $this->getNumbers($winNumbersIn, $cnt, $winNumbers);

        $wins = [];
        foreach ($winNumbers as $k => $hash) {
            if (isset($betNumbers[$k])) {
                $wins[] = $betNumbers[$k];
            }
        }
        return $wins;
    }

    function getNumbers($parts, $size, &$out, $partI = 0, $numI = 0, &$number = null)
    {


        for ($p = $partI; $p < $size; $p++) {
            for ($i = $numI; $i < count($parts); $i++) {
                if ($number) {
                    $k = $parts[$i] . ' ' . $number;
                } else {
                    $k = $parts[$i];
                }
                $this->getNumbers($parts, $size, $out, ($p + 1), ($i + 1), $k);

                $kArray = explode(" ", $k);

                if (count($kArray) == $size) {
                    $out[md5($k)] = $kArray;
                }
            }
        }
    }


}