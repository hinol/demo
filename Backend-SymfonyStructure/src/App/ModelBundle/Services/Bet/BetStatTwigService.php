<?php


namespace App\ModelBundle\Services\Bet;


use App\ModelBundle\Services\Core\CoreSettingService;
use App\ModelBundle\Services\Query\Bet\BetQueryService;
use App\ModelBundle\Services\Query\Bet\BetShareQueryService;
use App\ModelBundle\Services\Query\Bet\BetWinQueryService;
use Lib\Model\Bet;
use Lib\Model\User;

/**
 * Class BetStatTwigService
 * @package App\ModelBundle\Services\Bet
 */
class BetStatTwigService extends \Twig_Extension
{

    /**
     * @var BetQueryService
     */
    private $betQueryService;
    /**
     * @var BetShareQueryService
     */
    private $betShareQueryService;

    /**
     * @var CoreSettingService
     */
    private $coreSettingService;


    /**
     * @var BetWinQueryService
     */
    private $betWinQueryService;

    /**
     * @param BetQueryService $betQueryService
     * @param BetShareQueryService $betShareQueryService
     * @param CoreSettingService $coreSettingService
     * @param BetWinQueryService $betWinQueryService
     */
    public function __construct(
        BetQueryService $betQueryService,
        BetShareQueryService $betShareQueryService,
        CoreSettingService $coreSettingService,
        BetWinQueryService $betWinQueryService)
    {
        $this->betQueryService = $betQueryService;
        $this->betShareQueryService = $betShareQueryService;
        $this->coreSettingService = $coreSettingService;
        $this->betWinQueryService = $betWinQueryService;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'betStat' => new \Twig_Function_Method($this, 'betStat'),
        );
    }

    /**
     * @param Bet $bet
     * @param $action
     * @param User|null $user
     * @return int
     * @throws \PropelException
     */
    public function betStat(Bet $bet, $action, User $user = null)
    {
        switch ($action) {
            case "shares":
                if ($user == null) {
                    return $bet->getSharesCnt();
                }
                $query = $this->betShareQueryService->getQuery()
                    ->withColumn('SUM(cnt)', 'Total')
                    ->filterByBet($bet);
                if ($user) {
                    $query->filterByUser($user);
                }
                return $query->findOne()->getTotal();
            case "shareMiss":
                $maxShares = $this->coreSettingService->getMaxShare();
                return $maxShares - $this->betStat($bet, "shares");
            case "win":
                $row = $this->betWinQueryService->getQuery()->filterByBet($bet)->filterByUser($user)->findOne();
                if ($row) {
                    return $row->getValue();
                }
                return null;
            case "shareProgress":

                $maxShares = $this->coreSettingService->getMaxShare();
                return ($bet->getSharesCnt() / $maxShares) * 100;


        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "betStat";
    }
}