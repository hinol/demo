<?php


namespace App\ModelBundle\Services\Bet;


use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;

/**
 * Class GameTwigService
 * @package App\ModelBundle\Services\Bet
 */
class GameTwigService extends \Twig_Extension
{

    /**
     * @var BetGameService
     */
    private $betGameService;


    /**
     * @var CoreSettingService
     */
    private  $coreSettingService;
    /**
     * @param BetGameService $betGameService
     */
    public function __construct(BetGameService $betGameService, CoreSettingService $coreSettingService)
    {
        $this->betGameService = $betGameService;
        $this->coreSettingService=$coreSettingService;
    }
    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'nextGame' => new \Twig_Function_Method($this, 'nextGame', ["type"]),
            'nextGameWin' => new \Twig_Function_Method($this, 'nextGameWin', ["type"]),
        );
    }


    /**
     * @param string $type
     * @return \DateTime
     */
    public function nextGame($type=ConstService::LOTTO){
        return $this->betGameService->getNextGameDate($type);
    }
    /**
     * @param string $type
     * @return \DateTime
     */
    public function nextGameWin($type=ConstService::LOTTO){
        return $this->coreSettingService->getMainWin($type);
    }
    /**
     * @return string
     */
    public function getName()
    {
        return "game";
    }
}