<?php


namespace App\ModelBundle\Services\Bet;


use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use App\ModelBundle\Services\Finance\TransactionService;
use App\ModelBundle\Services\Query\Bet\BetQueryService;
use App\ModelBundle\Services\Query\Bet\BetShareQueryService;
use App\ModelBundle\Services\Query\User\UserQueryService;
use App\ToolBundle\Services\DatabaseService;
use Lib\Model\Bet;
use Lib\Model\BetShare;
use Lib\Model\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class BetShareService
 * @package App\ModelBundle\Services\Bet
 */
class BetShareService
{

    /**
     * @var BetShareQueryService
     */
    private $betShareQueryService;

    /**
     * @var CoreSettingService
     */
    private $coreSettingService;

    /**
     * @var TransactionService
     */
    private $transactionService;


    /**
     * @var TranslatorInterface
     */
    private $translatorInterface;

    /**
     * @var DatabaseService
     */
    private $databaseService;


    /**
     * @var UserQueryService
     */
    private $userQueryService;

    /**
     * @var BetQueryService
     */
    private $betQueryService;


    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcherInterface;

    /**
     * @param BetShareQueryService $betShareQueryService
     * @param CoreSettingService $coreSettingService
     * @param TransactionService $transactionService
     * @param TranslatorInterface $translatorInterface
     * @param DatabaseService $databaseService
     * @param UserQueryService $userQueryService
     * @param BetQueryService $betQueryService
     * @param EventDispatcherInterface $eventDispatcherInterface
     */
    public function __construct(
        BetShareQueryService $betShareQueryService,
        CoreSettingService $coreSettingService,
        TransactionService $transactionService,
        TranslatorInterface $translatorInterface,
        DatabaseService $databaseService,
        UserQueryService $userQueryService,
        BetQueryService $betQueryService,
        EventDispatcherInterface $eventDispatcherInterface
    )
    {
        $this->betShareQueryService = $betShareQueryService;
        $this->coreSettingService = $coreSettingService;
        $this->transactionService = $transactionService;
        $this->translatorInterface = $translatorInterface;
        $this->databaseService = $databaseService;
        $this->userQueryService = $userQueryService;
        $this->betQueryService = $betQueryService;
        $this->eventDispatcherInterface = $eventDispatcherInterface;
        BetShare::setEventDispatcher($this->eventDispatcherInterface);

    }


    /**
     * @param int $game
     * @param int $system
     * @return float
     */
    public function getSharePrice($game, $system)
    {
        return $this->coreSettingService->get("share_price")[$game][$system];
    }

    /**
     * @param string $game
     * @param int $system
     * @return float[]
     */
    public function getSharePrices($game, $system)
    {
        $basePrice = $this->getSharePrice($game, $system);
        $result = [];
        for ($i = 1; $i <= $this->coreSettingService->getMaxShare(); $i++) {
            $result[$i] = $basePrice * $i;
        }
        return $result;
    }

    /**
     * @param Bet $bet
     * @param User $user
     * @param int $cnt
     * @return $this|bool|BetShare
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function createBetShare(Bet $bet, User $user, $cnt = 0)
    {

        //you can buy 0 shares only to create row
        if ($cnt > 0) {

            $price = $this->getSharePrices($bet->getGameType(), $bet->getSystem())[$cnt];
        } else {
            $price = 0;
        }


        if (false == $this->transactionService->isValidTransaction($user, $price)) {
            return false;
        }

        $betShare = $this->addShares($cnt, $bet, $user);
        if ($price > 0) {
            $this->transactionService->newTransactionFromBetShare($betShare);
        }


        return $betShare;
    }


    /**
     * @param $cnt
     * @param Bet $bet
     * @param User $user
     * @return bool|BetShare
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function addShares($cnt, Bet $bet, User $user)
    {
        if($cnt > 0) {
            $price = $this->getSharePrices($bet->getGameType(), $bet->getSystem())[$cnt];
        }else{
            $price = 0;
        }

        if (false == $this->transactionService->isValidTransaction($user, $price)) {
            return false;
        }
        $this->databaseService->beginTransaction();

        //get current buy
        $currentShare = $this->betShareQueryService->getQuery()
            ->filterByBet($bet)
            ->filterByUser($user)
            ->findOneOrCreate();

        //add shares
        $currentShare->addCnt($cnt);
        $currentShare->addPrice($price);
        $currentShare->save();
        $transaction = $this->transactionService->newTransactionFromBetShare($currentShare, -1 * $price);


        $desc = $this->translatorInterface->trans("Shares count: %s. Bet: %s.");

        $transaction->setDescription(sprintf($desc, $cnt, $bet->getSlug()));
        $transaction->save();
        $this->databaseService->commit();
        return $currentShare;
    }


    /**
     * @return array
     */
    public function getAllSharePrices()
    {
        $games = [ConstService::LOTTO, ConstService::MINI_LOTTO];
        $systems = ConstService::getSystems();

        $result = [];
        foreach ($games as $game) {
            foreach ($systems as $system) {
                $result[$game][$system] = $this->getSharePrices($game, $system);
            }
        }
        return $result;
    }

    /**
     * @param $min
     * @param $max
     * @return bool|BetShare
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function generateBetShare($min, $max)
    {
        $user = $this->userQueryService->getRandomPlayer();
        $bet = $this->betQueryService->getRandomBet();
        $cnt = rand($min, $max);
        $price = $this->getSharePrices($bet->getGameType(), $bet->getSystem())[$cnt];
        $this->transactionService->newPayment($price + 1, $user);
        return $this->addShares($cnt, $bet, $user);
    }


    /**
     * @param GenericEvent $event
     * @return bool
     * @throws \Exception
     */
    public function preSave(GenericEvent $event)
    {

        $betShare = $event->getSubject();
        if ($betShare instanceof BetShare) {

            $bet = $betShare->getBet();
            if (null == $bet) {
                throw new \Exception("Bet share must to have bet");
            }
            $shares = 0;
            foreach ($bet->getBetShares() as $betShare2) {
                if ($betShare->getUser() != $betShare2->getUser()) {
                    $shares += $betShare2->getCnt();
                }
            }


            $nextCnt = $betShare->getCnt() + $shares;

            if ($nextCnt > $this->coreSettingService->getMaxShare()) {
                throw new \Exception("Bad cnt of shares - too many");
            }

            return true;
        } else {
            throw new \Exception("Bad event");
        }
    }

}