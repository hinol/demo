<?php


namespace App\ModelBundle\Services\Bet;


use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use App\ModelBundle\Services\Query\Bet\BetGameQueryService;
use App\ModelBundle\Services\Query\Bet\BetQueryService;
use App\ToolBundle\Services\DatabaseService;
use Lib\Model\Game;
use Lib\Model\Map\BetTableMap;
use Lib\Model\Map\GameTableMap;

/**
 * Class BetGameService
 * @package App\ModelBundle\Services\Bet
 */
class BetGameService
{

    /**
     * @var BetGameQueryService
     */
    private $betGameQueryService;

    /**
     * @var CoreSettingService
     */
    private $coreSettingService;

    /**
     * @var DatabaseService
     */
    private $databaseService;


    /**
     * @var BetService
     */
    private $betService;
    /**
     * @var BetQueryService
     */
    private $betQueryService;

    /**
     * @param BetGameQueryService $betGameQueryService
     * @param CoreSettingService $coreSettingService
     * @param DatabaseService $databaseService
     * @param BetService $betService
     * @param BetQueryService $betQueryService
     */
    public function __construct(
        BetGameQueryService $betGameQueryService,
        CoreSettingService $coreSettingService,
        DatabaseService $databaseService,
        BetService $betService,
        BetQueryService $betQueryService)
    {
        $this->betGameQueryService = $betGameQueryService;
        $this->coreSettingService = $coreSettingService;
        $this->databaseService = $databaseService;
        $this->betService = $betService;
        $this->betQueryService = $betQueryService;
    }

    /**
     * @param $type
     * @return \DateTime
     */
    public function getNextGameDate($type)
    {
        //day today
        $today = date("N");

        //get config for type
        $config = $this->coreSettingService->get(ConstService::SETTING_GAME_CALENDAR)[$type];

        //create date range for one week ahead for example
        // if today=2, days=[2,3,4,5,6,7,1,2,3,4,5,6,7]
        $days = range($today, 7);


        $days2 = range(1, 7);
        $days = array_merge($days, $days2);



        //get diff
        $days_diff = 0;
        foreach ($days as $day) {
            $days_diff++;
            if ($config[$day]) {
                break;
            }
        }


        //create next date
        $date = new \DateTime(date("Y-m-d") . ' ' . $config['hour']);
        $date->modify("+" . $days_diff . " days");
        return $date;
    }

    /**
     * @param $type
     * @return \DateTime|null
     */
    public function getTodayGameDate($type)
    {
        $config = $this->coreSettingService->get(ConstService::SETTING_GAME_CALENDAR)[$type];

        if ($config[date("N")]) {
            return new \DateTime(date("Y-m-d") . ' ' . $config['hour']);
        }

        return null;
    }

    /**
     * @return bool
     */
    public function generateGames()
    {
        $gameTypes = [ConstService::LOTTO, ConstService::MINI_LOTTO];

        $now = new \DateTime();
        foreach ($gameTypes as $gameType) {
            $date = $this->getTodayGameDate($gameType);

            if (
                $date < $now &&
                $this->betGameQueryService->getGameByDate($date->format("Y-m-d"))->count() === 0
            ) {
                $this->createGame($gameType, $date);
            }
        }
        return true;
    }

    private function createGame($gameType, \DateTime $date)
    {
        $this->databaseService->beginTransaction();
        $game = new Game();
        $game->setGameType($gameType);
        $game->setDate($date);

        $game->setHour($date->format("H:i"));
        $game->setStatus(GameTableMap::COL_STATUS_NEW);
        $game->save();

        $bets = $this->betQueryService->getBetsByGame($game);


        foreach ($bets as $bet) {
            $bet->setGame($game);
            $bet->setStatus(BetTableMap::COL_STATUS_WAIT);
            $bet->save();
        }

        $game->save();
        $this->databaseService->commit();
        return $game;
    }

    public function recountAssigned()
    {
        $this->databaseService->beginTransaction();
        $games = $this->betGameQueryService->getGamesToRecount();

        foreach ($games as $game) {
            foreach ($game->getBets() as $bet) {
                $this->betService->recount($bet);
            }
            $game->setStatus(GameTableMap::COL_STATUS_INPROGRESS);
            $game->save();
        }


        $this->databaseService->commit();
        return true;
    }
}