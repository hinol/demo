<?php


namespace App\ModelBundle\Services\Bet;


use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use App\ModelBundle\Services\Query\Bet\BetQueryService;
use App\ModelBundle\Services\Query\User\UserQueryService;
use App\ToolBundle\Services\DatabaseService;
use Lib\Model\Bet;
use Lib\Model\BetNumber;
use Lib\Model\BetNumberPeer;
use Lib\Model\BetShare;
use Lib\Model\Map\BetNumberTableMap;
use Lib\Model\Map\BetTableMap;
use Lib\Model\Map\GameTableMap;
use Lib\Model\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class BetService
 * @package App\ModelBundle\Services\Bet
 */
class BetService
{

    /**
     * @var BetQueryService
     */
    private $betQueryService;

    /**
     * @var BetShareService
     */
    private $betShareService;
    /**
     * @var DatabaseService
     */
    private $databaseService;

    /**
     * @var CoreSettingService
     */
    private $coreSettingService;


    /**
     * @var BetResultService
     */
    private $betResultService;

    /**
     * @var BetWinService
     */
    private $betWinService;

    /**
     * @var UserQueryService
     */
    private $userQueryService;


    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcherInterface;

    /**
     * @param BetQueryService $betQueryService
     * @param BetShareService $betShareService
     * @param DatabaseService $databaseService
     * @param CoreSettingService $coreSettingService
     * @param BetResultService $betResultService
     * @param BetWinService $betWinService
     * @param UserQueryService $userQueryService
     * @param EventDispatcherInterface $eventDispatcherInterface
     */
    public function __construct(
        BetQueryService $betQueryService,
        BetShareService $betShareService,
        DatabaseService $databaseService,
        CoreSettingService $coreSettingService,
        BetResultService $betResultService,
        BetWinService $betWinService,
        UserQueryService $userQueryService,
        EventDispatcherInterface $eventDispatcherInterface

    )
    {
        $this->betQueryService = $betQueryService;
        $this->betShareService = $betShareService;
        $this->databaseService = $databaseService;
        $this->coreSettingService = $coreSettingService;
        $this->betResultService = $betResultService;
        $this->betWinService = $betWinService;
        $this->userQueryService = $userQueryService;
        $this->eventDispatcherInterface = $eventDispatcherInterface;
        Bet::setEventDispatcher($this->eventDispatcherInterface);
    }


    /**
     * @param GenericEvent $event
     */
    public function preInsert(GenericEvent $event)
    {
        $bet = $event->getSubject();

        if ($bet instanceof Bet) {
            $this->generateSlug($bet);
            foreach ($bet->getBetShares() as $betShare) {
                $betShare->setUser($bet->getUser());
            }
        }
    }

    /**
     * @param GenericEvent $event
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function preSave(GenericEvent $event)
    {
        $bet = $event->getSubject();

        if ($bet instanceof Bet) {
            $sharePrice = $this->betShareService->getSharePrice($bet->getGameType(), $bet->getSystem());
            $bet->setSharePrice($sharePrice);
        }

    }

    /**
     * @param Bet $bet
     * @return Bet
     * @throws \PropelException
     */
    public function generateSlug(Bet $bet)
    {
        $slug = [];
        $slug[] = $bet->getGameType();
        $slug[] = $bet->getCreatedAt("Ymd");
        $todayBets = $this->betQueryService->getQuery()
            ->filterByCreatedAt([
                'min' => date("Y-m-d") . ' 00:00:00',
                'max' => date("Y-m-d") . ' 23:59:59',
            ])
            ->count();

        $slug[] = str_pad(++$todayBets, 4, '0', STR_PAD_LEFT);

        $bet->setSlug(strtoupper(implode("_", $slug)));
        return $bet;
    }

    /**
     * @param Bet $abstractBet
     * @param array $numbers
     * @param User $user
     * @param BetShare $betShare
     * @return Bet
     */
    public function newBet(Bet $abstractBet, array $numbers, User $user, BetShare $betShare)
    {

        $this->databaseService->beginTransaction();

        $bet = (new Bet())
            ->addNumbers($numbers)
            ->setUser($user)
            ->setGameType($abstractBet->getGameType())
            ->setSystem($abstractBet->getSystem());

        $this->generateSlug($bet);


        $bet->save();
//        if ($betShare->getCnt()) {
            $this->betShareService->createBetShare($bet, $user, $betShare->getCnt());
//        }
        $this->databaseService->commit();
        return $bet;
    }

    /**
     * @param Bet $bet
     * @return Bet
     * @throws \Exception
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function recount(Bet $bet)
    {
        if ($bet->getGame()->getNumberStatus() !== GameTableMap::COL_NUMBER_STATUS_FILLED) {
            throw new \Exception("Game number status mush be filled");
        }

        foreach ($bet->getGame()->getGameNumbers() as $gameNumber) {
            $betNumber = (new BetNumber())
                ->setNumber($gameNumber->getNumber())
                ->setBet($bet)
                ->setMode(BetNumberTableMap::COL_MODE_RESULT);
            $betNumber->save();
        }

        $betResult = $this->betResultService->createByBet($bet);

        $bet->setStatus(BetTableMap::COL_STATUS_RECOUNT);
        $bet->save();


        $this->betWinService->createByBet($bet);


        return $bet;
    }

    /**
     * @param $gameType
     * @param $system
     * @return Bet
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function generateBet($gameType, $system)
    {
        $user = $this->userQueryService->getRandomPlayer();
        $numbers = [];
        while (count($numbers) < $system) {
            $numbers[] = rand(0, ConstService::MAX_NUMBER);
            $numbers = array_unique($numbers);
        }

        $bet = new Bet();
        $bet
            ->setGameType($gameType)
            ->setSystem($system)
            ->setUser($user)
            ->setStatus(BetTableMap::COL_STATUS_NEW)
            ->addNumbers($numbers);
        $bet->save();
        return $bet;
    }


}