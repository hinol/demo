<?php


namespace App\ModelBundle\Services\Bet;


use App\ModelBundle\Services\Query\Bet\BetWinQueryService;
use Lib\Model\Bet;
use Lib\Model\BetWin;
use Lib\Model\Map\BetWinTableMap;

/**
 * Class BetWinService
 * @package App\ModelBundle\Services\Bet
 */
class BetWinService
{

    /**
     * @var BetWinQueryService
     */
    private $betWinQueryService;

    /**
     * @param BetWinQueryService $betWinQueryService
     */
    public function __construct(BetWinQueryService $betWinQueryService)
    {
        $this->betWinQueryService = $betWinQueryService;

    }

    public function createByBet(Bet $bet)
    {
        $shares = $bet->getSharesCnt();
        $totalWin = $bet->getBetResult()->getTotal();

        $winByShare = $totalWin / $shares;

        foreach ($bet->getBetShares() as $betShare) {
            (new BetWin())
                ->setBet($bet)
                ->setUser($betShare->getUser())
                ->setValue($winByShare * $betShare->getCnt())
                ->setStatus(BetWinTableMap::COL_STATUS_WAIT)
                ->save();
        }
        return true;
    }
}