<?php


namespace App\ModelBundle\Services\Query\Finance;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\FinanceTransactionQuery;

/**
 * Class TransactionQuery
 * @package App\ModelBundle\Services\Query\Finance
 */
class TransactionQueryService implements AbstractQueryInterface
{
    /**
     * @return FinanceTransactionQuery
     */
    public function getQuery()
    {
        return FinanceTransactionQuery::create();
    }
}