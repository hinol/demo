<?php


namespace App\ModelBundle\Services\Query\User;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\GroupQuery;
use Lib\Model\UserGroupQuery;

/**
 * Class UserGroupQueryService
 * @package App\ModelBundle\Services\Query\User
 */
class UserGroupQueryService implements AbstractQueryInterface
{
    /**
     * @return GroupQuery
     */
    public function getQuery()
    {
        return GroupQuery::create();
    }

    /**
     * @param int $page
     * @return \PropelModelPager
     */
    public function paginate($page = 1)
    {
        return $this->getQuery()->paginate($page);
    }

}