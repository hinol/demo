<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 26/05/15
 * Time: 16:18
 */

namespace App\ModelBundle\Services\Query\User;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use App\ModelBundle\Services\User\RoleService;
use Lib\Model\UserQuery;

/**
 * Class UserQueryService
 * @package App\ModelBundle\Services\Query\User
 */
class UserQueryService implements AbstractQueryInterface
{

    /**
     * @return UserQuery
     */
    public function getQuery()
    {
        return UserQuery::create();
    }

    /**
     * @param int $page
     * @return \PropelModelPager
     */
    public function paginate($page = 1)
    {
        return $this->getQuery()->paginate($page);
    }

    /**
     * @return \Lib\Model\User
     */
    public function getRandomPlayer()
    {
        $firstPlayer = $this->getQuery()->orderById(\Criteria::ASC)->filterByRole(RoleService::ROLE_PLAYER)->findOne()->getId();
        $lastPlayer = $this->getQuery()->orderById(\Criteria::DESC)->filterByRole(RoleService::ROLE_PLAYER)->findOne()->getId();
        $id = rand($firstPlayer, $lastPlayer);

        $user = $this->getQuery()->filterById($id, \Criteria::GREATER_THAN)->filterByRole(RoleService::ROLE_PLAYER)->findOne();
        if ($user == null) {
            return $this->getRandomPlayer();
        }
        return $user;
    }

    /**
     * @param array $filter
     * @return UserQuery
     */
    public function getQueryByFilter($filter)
    {
        if ($filter == null) {
            return $this->getQuery();
        }

        $q = $this->getQuery();
        foreach ($filter as $name => $value) {
            switch ($name) {
                case "name_like":
                    $q
                        ->filterByUsername($value, \Criteria::LIKE)
                        ->_or()
                        ->filterByFirstname($value, \Criteria::LIKE)
                        ->_or()
                        ->filterByLastname($value, \Criteria::LIKE);

                    break;
            }
        }
        return $q;
    }
}