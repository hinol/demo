<?php

namespace App\ModelBundle\Services\Query\User;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\UserHistoryQuery;

/**
 * Class UserHistoryQueryService
 * @package App\ModelBundle\Services\Query\User
 */
class UserHistoryQueryService implements AbstractQueryInterface
{
    /**
     * @return UserHistoryQuery
     */
    public function getQuery()
    {
        return UserHistoryQuery::create();
    }
}