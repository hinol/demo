<?php


namespace App\ModelBundle\Services\Query\User;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\UserDataQuery;

/**
 * Class UserDataQueryService
 * @package App\ModelBundle\Services\Query\User
 */
class UserDataQueryService implements AbstractQueryInterface
{

    /**
     * @return UserDataQuery
     */
    public function getQuery(){
        return UserDataQuery::create();
    }

}