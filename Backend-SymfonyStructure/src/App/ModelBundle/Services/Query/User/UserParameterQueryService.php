<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 10/07/15
 * Time: 13:54
 */

namespace App\ModelBundle\Services\Query\User;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\UserParameterQuery;

/**
 * Class UserParameterQueryService
 * @package App\ModelBundle\Services\Query\User
 */
class UserParameterQueryService implements AbstractQueryInterface
{
    /**
     * @return UserParameterQuery
     */
    public function getQuery()
    {
        return UserParameterQuery::create();
    }

}