<?php


namespace App\ModelBundle\Services\Query\Bet;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\BetShareQuery;

/**
 * Class BetShareQueryService
 * @package App\ModelBundle\Services\Query\Bet
 */
class BetShareQueryService implements AbstractQueryInterface
{
    /**
     * @return BetShareQuery
     */
    public function getQuery()
    {
        return BetShareQuery::create();
    }

}