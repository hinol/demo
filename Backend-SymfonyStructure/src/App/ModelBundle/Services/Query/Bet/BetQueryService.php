<?php


namespace App\ModelBundle\Services\Query\Bet;


use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Core\CoreSettingService;
use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\Bet;
use Lib\Model\BetQuery;
use Lib\Model\Game;
use Lib\Model\Map\BetTableMap;
use Lib\Model\User;

/**
 * Class BetQueryService
 * @package App\ModelBundle\Services\Query\Bet
 */
class BetQueryService implements AbstractQueryInterface
{
    /**
     * @var CoreSettingService
     */
    private $coreSettingService;

    /**
     * @param CoreSettingService $coreSettingService
     */
    public function __construct(CoreSettingService $coreSettingService)
    {
        $this->coreSettingService = $coreSettingService;
    }


    /**
     * @return BetQuery
     */
    public function getQuery()
    {
        return BetQuery::create();
    }

    /**
     * @param Game $game
     * @return \Lib\Model\Bet[]|\Propel\Runtime\Collection\ObjectCollection
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getBetsByGame(Game $game)
    {
        $maxShares = $this->coreSettingService->getMaxShare();

        return $this->getQuery()
            ->filterBySharesCnt($maxShares)
            ->filterByGameType($game->getGameType())
            ->find();
    }

    /**
     * @return Bet
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getRandomBet()
    {
        $firstPlayer = $this->getQuery()->orderById(\Criteria::ASC)->filterByStatus(BetTableMap::COL_STATUS_NEW)->findOne()->getId();
        $lastPlayer = $this->getQuery()->orderById(\Criteria::DESC)->filterByStatus(BetTableMap::COL_STATUS_NEW)->findOne()->getId();
        $id = rand($firstPlayer, $lastPlayer);

        $bet = $this->getQuery()->filterById($id, \Criteria::GREATER_THAN)->filterByStatus(BetTableMap::COL_STATUS_NEW)->findOne();
        if ($bet == null) {
            return $this->getRandomBet();
        }
        return $bet;
    }

    /**
     * @param $game
     * @param $system
     * @param $page
     * @return \Lib\Model\Bet[]|\Propel\Runtime\Util\PropelModelPager
     * @throws \Exception
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getBets($game, $system, $page)
    {
        if (false === array_key_exists($game, ConstService::getGamesAsAssocArray())) {
            throw new \Exception("Bad game type parameter");
        }
        if ($system != 'all' && false === array_key_exists($system, ConstService::getSystemsAsAssocArray())) {
            throw new \Exception("Bad system parameter");
        }

        $q = $this->getQuery()
            ->filterByGameType($game);

        if ($system != "all") {
            $q->filterBySystem($system);
        }


        return $q
            ->orderBySharesCnt(\Criteria::DESC)
            ->paginate($page);
    }

    public function getQueryByPlayer(User $user)
    {
        return $this->getQuery()
            ->useBetShareQuery()
            ->filterByUser($user)
            ->endUse();
    }

    public function getQueryByFilter($filter)
    {
        if (!is_array($filter)) {
            return $this->getQuery();
        }


        $q = $this->getQuery();
        foreach ($filter as $name => $value) {
            switch ($name) {
                case "game_type":
                    if ($value) {
                        $q->filterByGameType($value);
                    }

                    break;
                case "date_from":

                    $q->filterByCreatedAt($value, \Criteria::GREATER_THAN);
                    break;
                case "date_to":

                    $q->filterByCreatedAt($value, \Criteria::LESS_THAN);
                    break;
            }
        }
        return $q;
    }


}