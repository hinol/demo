<?php


namespace App\ModelBundle\Services\Query\Bet;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\BetResultQuery;

/**
 * Class BetResultQueryService
 * @package App\ModelBundle\Services\Query\Bet
 */
class BetResultQueryService implements AbstractQueryInterface
{
    /**
     * @return BetResultQuery
     */
    public function getQuery()
    {
        return BetResultQuery::create();
    }

}