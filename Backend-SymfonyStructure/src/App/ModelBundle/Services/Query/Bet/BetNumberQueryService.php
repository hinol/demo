<?php


namespace App\ModelBundle\Services\Query\Bet;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\BetNumberQuery;

/**
 * Class BetNumberQueryService
 * @package App\ModelBundle\Services\Query\Bet
 */
class BetNumberQueryService implements AbstractQueryInterface
{
    /**
     * @return BetNumberQuery
     */
    public function getQuery()
    {
        return BetNumberQuery::create();
    }

}