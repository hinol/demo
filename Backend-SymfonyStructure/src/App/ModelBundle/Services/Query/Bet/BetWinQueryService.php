<?php


namespace App\ModelBundle\Services\Query\Bet;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\BetWinQuery;

/**
 * Class BetWinQueryService
 * @package App\ModelBundle\Services\Query\Bet
 */
class BetWinQueryService implements AbstractQueryInterface
{
    /**
     * @return BetWinQuery
     */
    public function getQuery()
    {
        return BetWinQuery::create();
    }

}