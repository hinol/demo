<?php


namespace App\ModelBundle\Services\Query\Bet;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\GameQuery;
use Lib\Model\Map\GameTableMap;

/**
 * Class BetGameQueryService
 * @package App\ModelBundle\Services\Query\Bet
 */
class BetGameQueryService implements AbstractQueryInterface
{
    /**
     * @return GameQuery
     */
    public function getQuery()
    {
        return GameQuery::create();
    }

    /**
     * @param $format
     * @return \Lib\Model\Game[]|\Propel\Runtime\Collection\ObjectCollection
     */
    public function getGameByDate($format)
    {

        $dateFrom = new \DateTime($format . ' 00:00:00');
        $dateTo = new \DateTime($format . ' 23:59:59');

        return $this->getQuery()
            ->filterByDate(['min' => $dateFrom, 'max' => $dateTo])
            ->find();
    }

    /**
     * @return \Lib\Model\Game[]|\Propel\Runtime\Collection\ObjectCollection
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getGamesToRecount()
    {
        return $this->getQuery()->filterByStatus(GameTableMap::COL_STATUS_ASSIGNED)->find();
    }

}