<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 11/05/15
 * Time: 11:07
 */

namespace App\ModelBundle\Services\Query;


/**
 * Interface AbstractQueryInterface
 * @package App\ModelBundle\Services\Query
 */
interface AbstractQueryInterface
{

    /**
     * @return mixed
     */
    public function getQuery();
}