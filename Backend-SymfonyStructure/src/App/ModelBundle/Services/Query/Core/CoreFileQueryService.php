<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 11/05/15
 * Time: 11:53
 */

namespace App\ModelBundle\Services\Query\Core;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\CoreFileQuery;

/**
 * Class FileQueryService
 * @package App\ModelBundle\Services\Query
 */
class CoreFileQueryService implements  AbstractQueryInterface{


    /**
     * @return CoreFileQuery
     */
    public function getQuery(){
        return CoreFileQuery::create();
    }
}