<?php


namespace App\ModelBundle\Services\Query\Core;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\CoreMailQuery;

/**
 * Class CoreMailQueryService
 * @package App\ModelBundle\Services\Query\Core
 */
class CoreMailQueryService implements AbstractQueryInterface
{
    /**
     * @return CoreMailQuery
     */
    public function getQuery(){
        return CoreMailQuery::create();
    }

}