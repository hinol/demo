<?php


namespace App\ModelBundle\Services\Query\Core;


use App\ModelBundle\Services\Query\AbstractQueryInterface;
use Lib\Model\CoreSettingQuery;

/**
 * Class CoreSettingQueryService
 * @package App\ModelBundle\Services\Query\Core
 */
class CoreSettingQueryService implements AbstractQueryInterface
{
    /**
     * @return CoreSettingQuery
     */
    public function getQuery()
    {
        return CoreSettingQuery::create();
    }

}