<?php


namespace App\ModelBundle\Services\User;


use App\ModelBundle\Services\Query\User\UserDataQueryService;

/**
 * Class UserDataService
 * @package App\ModelBundle\Services\User
 */
class UserDataService
{

    /**
     * @var UserDataQueryService
     */
    private $userDataQueryService;

    /**
     * @param UserDataQueryService $userDataQueryService
     */
    public function __construct(UserDataQueryService $userDataQueryService)
    {
        $this->userDataQueryService = $userDataQueryService;
    }
}