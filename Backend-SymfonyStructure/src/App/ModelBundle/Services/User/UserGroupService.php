<?php


namespace App\ModelBundle\Services\User;


use App\ModelBundle\Services\Query\User\UserGroupQueryService;

/**
 * Class UserGroupService
 * @package App\ModelBundle\Services\User
 */
class UserGroupService
{

    /**
     * @var UserGroupQueryService
     */
    private $userGroupQueryService;

    /**
     * @param UserGroupQueryService $userGroupQueryService
     */
    public function __construct(UserGroupQueryService $userGroupQueryService)
    {
        $this->userGroupQueryService = $userGroupQueryService;
    }
}