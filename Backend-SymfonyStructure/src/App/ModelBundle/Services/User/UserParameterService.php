<?php


namespace App\ModelBundle\Services\User;


use App\ModelBundle\Services\Query\User\UserParameterQueryService;

/**
 * Class UserParameterService
 * @package App\ModelBundle\Services\User
 */
class UserParameterService
{

    /**
     * @var UserParameterQueryService
     */
    private $userParameterQueryService;

    /**
     * @param UserParameterQueryService $userParameterQueryService
     */
    public function __construct(UserParameterQueryService $userParameterQueryService)
    {
        $this->userParameterQueryService = $userParameterQueryService;
    }
}