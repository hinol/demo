<?php

namespace App\ModelBundle\Services\User;

use App\ModelBundle\Services\ConstService;
use App\ModelBundle\Services\Query\User\UserQueryService;
use App\ToolBundle\Services\SmsService;
use Buzz\Browser;
use FOS\UserBundle\Model\UserManagerInterface;
use Frontend\StaticBundle\Controller\SecurityController;
use Frontend\UserBundle\Controller\ActivateController;
use Lib\Model\Map\UserDataTableMap;
use Lib\Model\User;
use Lib\Model\UserDataPeer;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\RememberMe\AbstractRememberMeServices;

/**
 * Class UserService
 * @package App\ModelBundle\Services\User
 */
class UserService
{
    /**
     * @var UserQueryService
     */
    private $userQueryService;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var AbstractRememberMeServices
     */
    private $rememberMeServices;

    /**
     * @var UserManagerInterface
     */
    private $userManagerInterface;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var SmsService
     */
    private $smsService;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcherInterface;


    /**
     * @var Browser
     */
    private $browser;

    /**
     * @param UserQueryService $userQueryService
     * @param TokenStorage $tokenStorage
     * @param AbstractRememberMeServices $rememberMeServices
     * @param UserManagerInterface $userManagerInterface
     * @param RequestStack $requestStack
     * @param Router $router
     * @param SmsService $smsService
     * @param EventDispatcherInterface $eventDispatcherInterface
     * @param Browser $browser
     */
    function __construct(
        UserQueryService $userQueryService,
        TokenStorage $tokenStorage,
        AbstractRememberMeServices $rememberMeServices,
        UserManagerInterface $userManagerInterface,
        RequestStack $requestStack,
        Router $router,
        SmsService $smsService,
        EventDispatcherInterface $eventDispatcherInterface,
        Browser $browser
    )
    {
        $this->userQueryService = $userQueryService;
        $this->tokenStorage = $tokenStorage;
        $this->rememberMeServices = $rememberMeServices;
        $this->userManagerInterface = $userManagerInterface;
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->smsService = $smsService;
        $this->eventDispatcherInterface = $eventDispatcherInterface;
        $this->browser = $browser;

        //propel2 sf2 event hack :(
        User::setEventDispatcher($this->eventDispatcherInterface);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        $token = $this->tokenStorage->getToken();

        if ($token instanceof AnonymousToken) {
            return null;
        }

        if ($token) {
            return $token->getUser();
        }
        return null;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $user = $this->getUser();
        // if user player, and phone is not active, redirect to activation
        if (
            $user &&
            $user->isPlayer() &&
            $user->getUserData()->isPhoneActive() === false &&
            $event->getRequest()->get("_route") &&
            preg_match('/' . ActivateController::BASE_ROUTE . '/', $event->getRequest()->get("_route")) == false &&
            preg_match('/' . SecurityController::BASE_ROUTE . '/', $event->getRequest()->get("_route")) == false &&
            preg_match('/bet/', $event->getRequest()->getUri()) == true

        ) {
            $url = $this->router->generate(ActivateController::ROUTE_INDEX);
            $event->setResponse(new RedirectResponse($url));
        }

    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        if ($this->tokenStorage->getToken()) {
            $this->rememberMeServices->loginSuccess($event->getRequest(), $response, $this->tokenStorage->getToken());
        }
    }


    /**
     * @param GenericEvent $event
     * @return bool
     * @throws \Exception
     */
    public function preSave(GenericEvent $event)
    {

        $user = $event->getSubject();
        if ($user instanceof User) {
            $this->userManagerInterface->updatePassword($user);
            $this->userManagerInterface->updateCanonicalFields($user);
            return true;
        } else {
            throw new \Exception("Bad event");
        }
    }

    /**
     * @return string
     * @throws \Exception
     * @throws \PropelException
     */
    public function sendNewSmsCode()
    {

        $user = $this->getUser();
        $code = uniqid();
        $sendResult = $this->smsService->sendSms($user->getPhone(), ConstService::SMS_CONFIRM_CODE . ' ' . $code);
        if (true !== $sendResult) {
            throw new \Exception(sprintf("Sms sms failure, number %s, code %s", $user->getPhone(), $code));
        }

        $userData = $user->getUserData();
        $userData->setPhoneCode($code);
        $userData->setPhoneStatus(UserDataTableMap::COL_PHONE_STATUS_INPROGRESS);
        $userData->save();

        return $code;
    }

    /**
     * @param $codeToCheck
     * @return bool
     * @throws \Exception
     * @throws \PropelException
     */
    public function checkPhoneCode($codeToCheck)
    {
        $user = $this->getUser();
        if ($codeToCheck == $user->getUserData()->getPhoneCode()) {
            $user->getUserData()
                ->setPhoneCode(null)
                ->setPhoneStatus(UserDataTableMap::COL_PHONE_STATUS_ACTIVE)
                ->save();
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function loginForce(User $user)
    {
        $user->setLastLogin(new \DateTime());
        $user->save();
        $userPasswordToken = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->tokenStorage->setToken($userPasswordToken);

        return true;
    }

    public function generatePlayer()
    {
        $user = $this->generateUser();
        $user->setPlayer();
        $user->save();
        return $user;

    }


    /**
     * @param int $try
     * @return User
     * @throws \Exception
     */
    private function generateUser($try = 0)
    {
        if ($try > 10) {
            throw new \Exception("Some error");
        }
        $user = new User();
        $user
            ->setFirstname("Maciek" . uniqid())
            ->setLastname("Budzien" . uniqid())
            ->setEmail("maciek.budzien" . uniqid() . "@example.com")
            ->setPhone(rand(1000000, 999999999))
            ->setPlainPassword(uniqid())
            ->setEnabled(true);

        try {
            $user->save();
        } catch (\Exception $e) {
            return $this->generateUser(++$try);
        }

        return $user;
    }

}