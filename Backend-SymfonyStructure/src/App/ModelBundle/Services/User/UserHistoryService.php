<?php


namespace App\ModelBundle\Services\User;


use App\ModelBundle\Services\Query\User\UserHistoryQueryService;

/**
 * Class UserHistoryService
 * @package App\ModelBundle\Services\User
 */
class UserHistoryService
{
    /**
     * @var UserHistoryQueryService
     */
    private $userHistoryQueryService;

    /**
     * @param UserHistoryQueryService $userHistoryQueryService
     */
    function __construct(UserHistoryQueryService $userHistoryQueryService)
    {
        $this->userHistoryQueryService = $userHistoryQueryService;
    }
}