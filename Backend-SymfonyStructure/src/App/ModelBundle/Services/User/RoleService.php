<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 28/05/15
 * Time: 13:45
 */

namespace App\ModelBundle\Services\User;


class RoleService
{


    const ROLE_PANEL = "ROLE_PANEL";
    const ROLE_PLAYER = "ROLE_PLAYER";


    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    const ROLE_USER_TRANSLATE = 'ROLE_USER_TRANSLATE';
    const ROLE_USER_ORDER = 'ROLE_USER_ORDER';
    const ROLE_USER_SELLER = 'ROLE_USER_SELLER';
    const ROLE_USER_FINANCE = 'ROLE_USER_FINANCE';
    const ROLE_USER_PANEL = 'ROLE_USER_PANEL';
    const ROLE_ADMIN_ORDER = 'ROLE_ADMIN_ORDER';

    const ROLE_PANEL_GAME_CALENDAR = 'ROLE_PANEL_GAME_CALENDAR';
    const ROLE_PANEL_TICKET_PRICE = 'ROLE_PANEL_TICKET_PRICE';
    const ROLE_PANEL_SHARE_PRICE = 'ROLE_PANEL_SHARE_PRICE';
    const ROLE_PANEL_COUPON_CLOSE = 'ROLE_PANEL_COUPON_CLOSE';
    const ROLE_PANEL_COUPON_MAP = 'ROLE_PANEL_COUPON_MAP';
    const ROLE_PANEL_AUTO_PLAYER = 'ROLE_PANEL_AUTO_PLAYER';
    static function getRolesAsAssocArray()
    {
        return [
            self::ROLE_USER => self::ROLE_USER,
            self::ROLE_ADMIN => self::ROLE_ADMIN,
            self::ROLE_SUPER_ADMIN => self::ROLE_SUPER_ADMIN,
            self::ROLE_USER_TRANSLATE => self::ROLE_USER_TRANSLATE,
            self::ROLE_USER_ORDER => self::ROLE_USER_ORDER,
            self::ROLE_USER_SELLER => self::ROLE_USER_SELLER,
            self::ROLE_USER_FINANCE => self::ROLE_USER_FINANCE,
            self::ROLE_USER_PANEL => self::ROLE_USER_PANEL,
            self::ROLE_ADMIN_ORDER => self::ROLE_ADMIN_ORDER,
            self::ROLE_PANEL_GAME_CALENDAR => self::ROLE_PANEL_GAME_CALENDAR
        ];
    }


    static function getPanelRolesAsAssocArray()
    {
        return [


            self::ROLE_PANEL_GAME_CALENDAR => self::ROLE_PANEL_GAME_CALENDAR,
            self::ROLE_PANEL_TICKET_PRICE => self::ROLE_PANEL_TICKET_PRICE,
            self::ROLE_PANEL_SHARE_PRICE => self::ROLE_PANEL_SHARE_PRICE,
            self::ROLE_PANEL_COUPON_CLOSE => self::ROLE_PANEL_COUPON_CLOSE,
            self::ROLE_PANEL_COUPON_MAP => self::ROLE_PANEL_COUPON_MAP,
            self::ROLE_PANEL_AUTO_PLAYER => self::ROLE_PANEL_AUTO_PLAYER,

        ];
    }

}