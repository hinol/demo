<?php


namespace App\ModelBundle\Services\Coupon;


use App\ModelBundle\Services\Core\CoreSettingService;

/**
 * Class CouponService
 * @package App\ModelBundle\Services\Coupon
 */
class CouponService
{


    /**
     * @var CoreSettingService
     */
    private $coreSettingService;

    /**
     * @param CoreSettingService $coreSettingService
     */
    public function __construct(CoreSettingService $coreSettingService)
    {
        $this->coreSettingService = $coreSettingService;
    }

    /**
     * @param int $system
     * @param int[] $numbers
     * @return \Imagick
     * @throws \Exception
     */
    public function generateCoupon($system, $numbers)
    {
        if (false === in_array($system, range(7, 12))) {
            throw new \Exception("Bad system");
        }
        $availableNumbers = range(1, 46);
        foreach ($numbers as $number) {
            if (false === in_array($number, $availableNumbers)) {
                throw new \Exception("Bad number");
            }
        }


        $config = $this->coreSettingService->get("coupon_map");
        $systemConfig = $config['system' . $system];

        $c = new \Imagick();
        $c->newImage($config['coupon_width'], $config['coupon_height'], new \ImagickPixel("white"));
        $c->setFormat("png");

        foreach ($numbers as $number) {
            $numberConfig = $config[$number];

            $width = $numberConfig['width'];
            if ($width > 0) {
                for ($i = 0; $i <= $width; $i++) {
                    $draw = new \ImagickDraw();
                    $draw->setStrokeWidth(1);
                    $draw->setFillColor("black");
                    $draw->line($numberConfig['x'] + $i - 1, $numberConfig['y'] - 1, $numberConfig['x'] + $i - 1, ($numberConfig['y'] + $numberConfig['length'] - 1));
                    $c->drawImage($draw);
                }
            }
        }

        $numberConfig = $systemConfig;
        $width = $numberConfig['width'];
        if ($width > 0) {
            for ($i = 0; $i <= $width; $i++) {
                $draw = new \ImagickDraw();
                $draw->setStrokeWidth(1);
                $draw->setFillColor("black");
                $draw->line($numberConfig['x'] + $i - 1, $numberConfig['y'] - 1, $numberConfig['x'] + $i - 1, ($numberConfig['y'] + $numberConfig['length'] - 1));
                $c->drawImage($draw);
            }
        }


        return $c;
    }
}