<?php



namespace App\ModelBundle\Propel\Behavior;

/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 05/05/15
 * Time: 13:11
 */
class SeoBehavior extends \Behavior
{
    // default parameters value
    protected $parameters = array(
        'seo_title_column' => 'seo_title',
        'seo_description_column' => 'seo_description',
        'seo_robots_column' => 'seo_robots',

    );

    protected function getColumnSetter($column)
    {
        return 'set' . $this->getColumnForParameter($column)->getPhpName();
    }

    protected function getColumnConstant($columnName, OMBuilder $builder)
    {
        return $builder->getColumnConstant($this->getColumnForParameter($columnName));
    }
    public function modifyTable()
    {
        if (!$this->getTable()->containsColumn($this->getParameter('seo_title_column'))) {
            $this->getTable()->addColumn(array(
                'name' => $this->getParameter('seo_title_column'),
                'type' => 'varchar',
                "size" => "250"

            ));
        }

        if (!$this->getTable()->containsColumn($this->getParameter('seo_description_column'))) {
            $this->getTable()->addColumn(array(
                'name' => $this->getParameter('seo_description_column'),
                'type' => 'varchar',
                "size" => "250"

            ));
        }

        if (!$this->getTable()->containsColumn($this->getParameter('seo_robots_column'))) {
            $this->getTable()->addColumn(array(
                'name' => $this->getParameter('seo_robots_column'),
                'type' => 'ENUM',
                "valueSet" => "indexfollow, noindexfollow, noindexnofollow"

            ));
        }

    }
}