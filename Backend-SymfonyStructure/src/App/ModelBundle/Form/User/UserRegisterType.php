<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 02/06/15
 * Time: 16:23
 */

namespace App\ModelBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegisterType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("Firstname");
        $builder->add("Email");
        $builder->add("Password");

    }

    /** @inheritdoc */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lib\Model\User',
        ));
    }

    /** @inheritdoc */
    public function getName()
    {
        return "user_register";
    }
}