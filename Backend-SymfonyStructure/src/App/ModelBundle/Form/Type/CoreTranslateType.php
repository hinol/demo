<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 02/06/15
 * Time: 08:09
 */

namespace App\ModelBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CoreTranslateType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("TranslatedValue", "text");
    }

    /** @inheritdoc */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lib\Model\CoreTranslate',
        ));
    }

    /** @inheritdoc */
    public function getName()
    {
        return "core_translate_type";
    }

}