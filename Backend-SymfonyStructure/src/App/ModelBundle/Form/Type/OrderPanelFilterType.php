<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 02/06/15
 * Time: 13:50
 */

namespace App\ModelBundle\Form\Type;

use Lib\Model\OrderPeer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderPanelFilterType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $v = OrderPeer::getValueSets()[OrderPeer::STATUS];
        $values = array_combine($v, $v);

        $builder->add("Status", "choice", [
            'choices' => $values,
            'required'=>true,
            'data'=>OrderPeer::STATUS_NEW,
        ]);
    }

    /** @inheritdoc */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }

    /** @inheritdoc */
    public function getName()
    {
        return "order_panel_filter";
    }
}