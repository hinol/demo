<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 19/05/15
 * Time: 10:56
 */

namespace App\ModelBundle\Request\ParamConverter;


use App\ModelBundle\Services\Query\User\UserGroupQueryService;
use Lib\Model\Group;
use Lib\Model\Order;
use Lib\Model\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class UserConverter
 * @package App\ModelBundle\Request\ParamConverter
 */
class GroupConverter implements ParamConverterInterface
{


    /**
     * @var string
     */
    const PARAM_NAME = 'group';
    /**
     * @var string
     */
    const PARAM_CLASS = Group::class;


    /**
     * @var UserGroupQueryService
     */
    private $userGroupQueryService;


    public function __construct(UserGroupQueryService $userGroupQueryService)
    {
        $this->userGroupQueryService = $userGroupQueryService;
    }

    /** @inheritdoc */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $request->attributes->get(self::PARAM_NAME);

        $product = null;
        if (is_numeric($param)) {
            $product = $this->userGroupQueryService->getQuery()->findOneById($param);
        }

        if (null !== $product) {
            $request->attributes->set(self::PARAM_NAME, $product);
            return true;
        }
        return false;
    }

    /** @inheritdoc */
    public function supports(ParamConverter $configuration)
    {

        if ($configuration->getName() == self::PARAM_NAME && $configuration->getClass() == self::PARAM_CLASS) {
            return true;
        }
        return false;

    }
}