<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 19/05/15
 * Time: 10:56
 */

namespace App\ModelBundle\Request\ParamConverter;


use App\ModelBundle\Services\Query\Bet\BetQueryService;
use Lib\Model\Bet;
use Lib\Model\Order;
use Lib\Model\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class UserConverter
 * @package App\ModelBundle\Request\ParamConverter
 */
class BetConverter implements ParamConverterInterface
{


    /**
     * @var string
     */
    const PARAM_NAME = 'bet';
    /**
     * @var string
     */
    const PARAM_CLASS = Bet::class;


    private $betQueryService;


    public function __construct(BetQueryService $betQueryService)
    {
        $this->betQueryService = $betQueryService;
    }

    /** @inheritdoc */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $request->attributes->get(self::PARAM_NAME);

        $product = null;
        if (is_numeric($param)) {
            $product = $this->betQueryService->getQuery()->findOneById($param);
        }

        if (null !== $product) {
            $request->attributes->set(self::PARAM_NAME, $product);
            return true;
        }
        return false;
    }

    /** @inheritdoc */
    public function supports(ParamConverter $configuration)
    {

        if ($configuration->getName() == self::PARAM_NAME && $configuration->getClass() == self::PARAM_CLASS) {
            return true;
        }
        return false;

    }
}