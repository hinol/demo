<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 11/05/15
 * Time: 12:06
 */

namespace App\ToolBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Lib\Crawler\AliExpress\CrawlerAliExpress;
use Symfony\Component\DependencyInjection\ContainerInterface;


class CrawlerCommand extends ContainerAwareCommand
{

    /**
     * @var CrawlerAliExpress
     */
    private $crawler;

    /**
     * @var ContainerInterface
     */
    private $container;


    protected function configure()
    {
        $this
            ->setName('tool:crawler')
            ->setDescription('AliExpress Crawler')
            ->addArgument(
                'query',
                InputArgument::REQUIRED,
                'Query Serarch'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getContainer();

        $this->crawler = $this->container->get("tool.crawler.aliexpress")->getCrawler();

        $query = $input->getArgument('query');


        $products = $this->crawler->setQuery($query)->getProducts();


        var_dump($products);


        die;
    }
}