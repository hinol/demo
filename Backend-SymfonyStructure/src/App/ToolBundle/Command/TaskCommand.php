<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 20/05/15
 * Time: 16:13
 */

namespace App\ToolBundle\Command;

use Lib\Crawler\AliExpress\CrawlerAliExpress;
use Lib\Model\CoreTask;
use Lib\Model\CoreTaskPeer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TaskCommand extends ContainerAwareCommand
{
    /**
     * @var CrawlerAliExpress
     */
    private $crawler;

    /**
     * @var ContainerInterface
     */
    private $container;


    protected function configure()
    {
        $this
            ->setName('tool:task')
            ->setDescription('Run tasks');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getContainer();


        $task = $this->container->get("query.core.task")->getQuery()
            ->filterByStatus(CoreTaskPeer::STATUS_TODO)
            ->findOne();

        if (null === $task) {
            $output->writeln("No task to do");
            die;
        }
        if ($task instanceof CoreTask) {

            $output->writeln(sprintf("Start %s", $task->getName()));
            $output->writeln(sprintf("Parameters: %s", $task->getParameter()));

            $task->inProgress();
            try {
                $command = $this->getApplication()->find($task->getName());

                $arguments = explode(" ", trim($task->getParameter()));
                $argumentsName = array_keys($command->getDefinition()->getArguments());
                $arguments = array_combine($argumentsName, $arguments);
                $arguments['command'] = $task->getName();


                $arguments['--create_task']= true;

                $commandInput = new ArrayInput($arguments);




                $command->run($commandInput, $output);

                $task->finish();
            } catch (\Exception $e) {
                $task->toDo();

                $task->setData(['error' => $e->getMessage()]);
                $output->writeln("Error: " . $e->getMessage());
            }


        }

    }
}