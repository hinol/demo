<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 13/05/15
 * Time: 10:57
 */

namespace App\ToolBundle\Command\Aliexpress;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Lib\Crawler\AliExpress\CrawlerAliExpress;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CategoryCommand extends ContainerAwareCommand
{
    /**
     * @var CrawlerAliExpress
     */
    private $crawler;

    /**
     * @var ContainerInterface
     */
    private $container;


    protected function configure()
    {
        $this
            ->setName('tool:aliexpress:category')
            ->setDescription('AliExpress Category Crawler')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getContainer();

        $this->container->get("tool.crawler.aliexpress")->createCategories();






        die;
    }
}