<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 13/05/15
 * Time: 10:57
 */

namespace App\ToolBundle\Command\Aliexpress;

use App\ModelBundle\Services\Product\ProductService;
use Lib\Crawler\AliExpress\CrawlerAliExpress;
use Lib\Model\CoreTask;
use Lib\Model\Shop;
use Lib\Model\ShopQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShopCommand extends ContainerAwareCommand
{
    const OPITION = 'option';
    const NAME = "tool:aliexpress:shop";
    const TASK = "create_task";

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @var integer
     */
    private $shopId;


    /**
     * @var Shop
     */
    private $shopModel;

    /**
     * @var CrawlerAliExpress
     */
    private $crawlerAliExpress;

    /**
     * @var CrawlerAliExpressService
     */
    private $crawlerAliExpressService;

    /**
     * @var ShopQuery
     */
    private $shopQuery;

    /** {@inheritdoc} */
    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('AliExpress Shop Command')
            ->addArgument(
                'shop_id',
                InputArgument::REQUIRED,
                'Shop id from aliexpress'
            )
            ->addArgument(
                'action',
                InputArgument::REQUIRED,
                'Action: create|updateCategory|updateProducts'
            )
            ->addArgument(
                self::OPITION,
                InputArgument::OPTIONAL,
                "optional argument for example page"
            )
            ->addOption(self::TASK, null, InputOption::VALUE_NONE, "create next task or not");
    }

    /** {@inheritdoc} */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getContainer();


        $this->shopQuery = $this->container->get("query.shop")->getQuery();
        $this->shopModel = $this->container->get("model.shop");

        $this->crawlerAliExpressService = $this->container->get("tool.crawler.aliexpress");
        $this->crawlerAliExpress = $this->crawlerAliExpressService->getCrawler();

        /**
         * @var ProductService
         */
        $productService = $this->container->get("model.product");


        $this->shopId = $input->getArgument("shop_id");


        $action = $input->getArgument("action");
        switch ($action) {
            case "create":
                $shopData = $this->crawlerAliExpress->getShopData($this->shopId);
                $this->shopModel->createFromAliexpressShop($shopData);

                break;
            case "updateCategory":
                $this->crawlerAliExpressService->createCategoriesByShopId($this->shopId);
                break;
            case "updateProducts":
                $page = 1;
                if ($input->getArgument(self::OPITION)) {
                    $page = $input->getArgument(self::OPITION);
                }
                $output->writeln(sprintf("Download data for %s, page: %s", $this->shopId, $page));
                $products = $this->crawlerAliExpress->getProductsByShopId($this->shopId, $page);
                foreach ($products as $product) {
                    $output->writeln(sprintf("Add/Update product: %s, %s", substr($product->getTitle(), 0, 20), $product->getUrl()));
                    $productService->createFromAliexpressProduct($product);
                }
                if (count($products) && $input->getOption(self::TASK)) {
                    $task = (new CoreTask())
                        ->setName(self::NAME)
                        ->setParameter($this->shopId . ' ' . $action . ' ' . ++$page);
                    $task->toDo();
                }


                break;
        }

    }
}