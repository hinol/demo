<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 19/05/15
 * Time: 11:56
 */

namespace App\ToolBundle\Twig;


use App\ModelBundle\Services\Core\CoreFileService;
use Lib\Model\CoreFile;

/**
 * Class CoreFileExtension
 * @package App\ToolBundle\Twig
 */
class CoreFileExtension extends \Twig_Extension
{

    /**
     * @var CoreFileService
     */
    private $fileService;

    /**
     * @param CoreFileService $fileService
     */
    function __construct(CoreFileService $fileService)
    {
        $this->fileService = $fileService;
    }


    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'core_file' => new \Twig_Function_Method($this, 'coreFile')
        );
    }


    /**
     * @param CoreFile $file
     * @param $size
     * @return string
     */
    public function coreFile(CoreFile $file, $size)
    {
        return $this->fileService->getUrl($file, $size);

    }

    /** @inheritdoc */
    public function getName()
    {
        return "core_file";
    }
}