<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 19/05/15
 * Time: 11:56
 */

namespace App\ToolBundle\Twig;


use App\ModelBundle\Services\CmsPageService;
use App\ModelBundle\Services\Ecommerce\CartService;
use App\ModelBundle\Services\Product\Define\ProductDefineCategoryService;
use App\ToolBundle\Services\CacheService;
use Lsw\MemcacheBundle\Cache\MemcacheInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class PartialExtension
 * @package App\ToolBundle\Twig
 */
class PartialExtension extends \Twig_Extension
{

    /**
     * @var CmsPageService
     */
    private $cmsPageService;


    /**
     * @var ProductDefineCategoryService
     */
    private $productDefineCategoryService;
    /**
     * @var CartService
     */
    private $cartService;
    /**
     * @var CacheService
     */
    private $cache;

    /**
     * @param CmsPageService $cmsPageService
     * @param ProductDefineCategoryService $productDefineCategoryService
     * @param CacheService $cacheService
     * @param \Twig_Environment $twig
     * @param CartService $cartService
     */
    function __construct(
        CmsPageService $cmsPageService,
        CacheService $cacheService,
        \Twig_Environment $twig
    )
    {
        $this->cmsPageService = $cmsPageService;
        $this->cache = $cacheService;
        $this->twig = $twig;
    }


    /**
     * @return array
     */
    public
    function getFunctions()
    {
        return array(
            'partial' => new \Twig_Function_Method($this, 'partial', ['is_safe' => ['html']])
        );
    }


    /**
     * @param $name
     * @return mixed|string
     * @throws \Exception
     */
    public function partial($name)
    {
        $cacheKey = $name;
        $response = null;
        $cacheTime = 0;

        switch ($name) {
            case "CategoryMenu":
            case "CategoryBreadcrumb":
                $activeCategory = $this->productDefineCategoryService->getProductActiveDefineCategory();
                $cacheKey = $name . ($activeCategory ? $activeCategory->getId() : 'null');
                $cacheTime = 100;
                break;
            case "FooterMenu":
            case "TopMenu":
            case "SearchTop":
                $cacheTime = 10;
                break;


        }


        if ($cacheKey && $cacheTime) {
            if ($this->cache->is($cacheKey)) {
                return $this->cache->get($cacheKey);
            }

        }

        $twig = $this->twig;
        switch ($name) {
            case "TopMenu":
                $response = $twig->render("@FrontendStatic/Partials/topMenu.html.twig", ['pages' => $this->cmsPageService->getTreeByName("TopMenuRoot")]);
                break;

            case "CategoryMenu":
                $response = $twig->render("@FrontendStatic/Partials/tree.html.twig", $this->productDefineCategoryService->getSidebarTreeParameters());
                break;

            case "FooterMenu":
                $response = $twig->render("@FrontendStatic/Partials/footerMenu.html.twig", ['pages' => $this->cmsPageService->getTreeByName("FooterMenuRoot")]);
                break;

            case "CategoryBreadcrumb":
                $activeCategory = $this->productDefineCategoryService->getProductActiveDefineCategory();
                if (null == $activeCategory) {
                    return '';
                }

                $response = $twig->render("@FrontendStatic/Partials/categoryBreadcrumb.html.twig", ['category' => $activeCategory]);
                break;
            case "SearchTop":
                $response = $twig->render("@FrontendStatic/Partials/searchTop.html.twig");
                break;
            case "User/TopMenu":
                $response = $twig->render("@FrontendStatic/Partials/user/topMenu.html.twig");
                break;

            case "Ecommerce/TopMenu":
                $response = $twig->render("@FrontendStatic/Partials/ecommerce/cartSmall.html.twig", ['cart' => $this->cartService->getCart()]);
                break;


            default:
                throw new \Exception(sprintf("Partial %s not defined", $name));
                break;
        }


        if ($cacheTime) {
            $this->cache->set($cacheKey, $response, $cacheTime);
        }

        return $response;


    }

    /** @inheritdoc */
    public
    function getName()
    {
        return "partial";
    }
}