<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 19/05/15
 * Time: 12:06
 */

namespace App\ToolBundle\Controller;

use App\ModelBundle\Services\Core\CoreFileService;
use JMS\DiExtraBundle\Annotation\Inject;
use Lib\Model\CoreFile;
use Lib\Model\File;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileController extends Controller
{


    /**
     * @var CoreFileService
     * @Inject("model.core.file")
     */
    private $fileService;

    /**
     * @param CoreFile $file
     * @param $size
     * @Route("/uploads/file/{file}/{size}/file.jpg", name="core_file")
     * @ParamConverter("file", class="Lib\Model\CoreFile", options={"mapping"={"file":"id"}})
     * @return Response
     */
    public function indexAction(CoreFile $file, $size, Request $request)
    {

        $image = $this->fileService->getImage($file, $size, $request);


        return new Response($image, 200, ['Content-Type' => $file->getMimetype()]);
    }
}