<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 11/05/15
 * Time: 12:13
 */

namespace App\ToolBundle\Services;


use App\ModelBundle\Services\Product\Define\ProductDefineCategoryService;
use App\ModelBundle\Services\Query\Product\ProductDefineCategoryQueryService;
use App\ModelBundle\Services\Query\Product\ProductDefineParameterQueryService;
use Buzz\Browser;
use Lib\Crawler\AliExpress\CrawlerAliExpress;
use Lib\Model\ProductDefineCategory;
use Lsw\MemcacheBundle\Cache\MemcacheInterface;

/**
 * Class CrawlerAliExpressService
 * @package App\ToolBundle\Services
 */
class   CrawlerAliExpressService
{

    /**
     * @var Browser
     */
    private $browser;

    /**
     * @var MemcacheInterface
     */
    private $cache;

    /**
     * @var ProductDefineCategoryService
     */
    private $productDefineCategoryService;

    /**
     * @var ProductDefineCategoryQueryService
     */
    private $productDefineCategoryQueryService;

    /**
     * @param Browser $browser
     * @param MemcacheInterface $memcache
     * @param ProductDefineCategoryService $productDefineCategoryService
     * @param ProductDefineCategoryQueryService $productDefineCategoryQueryService
     */
    public function __construct(Browser $browser, MemcacheInterface $memcache, ProductDefineCategoryService $productDefineCategoryService, ProductDefineCategoryQueryService $productDefineCategoryQueryService)
    {
        $this->browser = $browser;
        $this->cache = $memcache;
        $this->productDefineCategoryService = $productDefineCategoryService;
        $this->productDefineCategoryQueryService = $productDefineCategoryQueryService;
    }


    /**
     * @return CrawlerAliExpress
     */
    public function getCrawler()
    {
        return (new CrawlerAliExpress())
            ->setBrowser($this->browser)
            ->setCache($this->cache);
    }

    /**
     * @throws \Exception
     * @throws \PropelException
     */
    public function createCategories()
    {

        $aliexpressCategories = $this->getCrawler()->getCategoriesFromMobile();

        $pdo = \Propel::getConnection();
        $pdo->exec("delete from Product__Define__Category where root_id=0");

        $query = $this->productDefineCategoryQueryService->getQuery();

        $root = $query->findRoot(0);
        if (null === $root) {
            $root = (new ProductDefineCategory())
                ->makeRoot()
                ->setName("MAIN ROOT")
                ->setScopeValue(0);
            $root->save();
        }

        foreach ($aliexpressCategories as $aliexpressCategory) {
            $this->productDefineCategoryService->createFromAliexpressCategory($aliexpressCategory, $root);
        }
    }

    /**
     * @param $shopId
     * @throws \Exception
     * @throws \PropelException
     */
    public function createCategoriesByShopId($shopId)
    {
        $aliexpressCategories = $this->getCrawler()->getCategoriesForShop($shopId);

        $query = $this->productDefineCategoryQueryService->getQuery();
        $root = $query->findRoot($shopId);
        if (null === $root) {
            $root = (new ProductDefineCategory())
                ->makeRoot()
                ->setName("ROOT SHOP " . $shopId)
                ->setScopeValue($shopId);
            $root->save();
        }

        foreach ($aliexpressCategories as $aliexpressCategory) {
            $this->productDefineCategoryService->createFromAliexpressCategory($aliexpressCategory, $root);
        }
    }
}