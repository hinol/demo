<?php


namespace App\ToolBundle\Services;


use App\ModelBundle\Services\ConstService;
use Ras\Bundle\FlashAlertBundle\Model\AlertReporter;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class FlashService
 * @package App\ToolBundle\Services
 */
class FlashService
{

    /**
     * @var AlertReporter
     */
    private $alertReporter;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param AlertReporter $alertReporter
     * @param TranslatorInterface $translator
     */
    public function __construct(AlertReporter $alertReporter, TranslatorInterface $translator)
    {
        $this->alertReporter = $alertReporter;
        $this->translator = $translator;
    }

    /**
     * @param $message
     */
    public function addError($message)
    {
        $this->alertReporter->addError($this->translator->trans($message));
    }


    /**
     * @param $message
     */
    public function addSuccess($message)
    {
        $this->alertReporter->addSuccess($this->translator->trans($message));

    }


    /**
     * @param $message
     */
    public function addInfo($message)
    {
        $this->alertReporter->addInfo($this->translator->trans($message));
    }


    /**
     * @param $message
     */
    public function addWarning($message)
    {
        $this->alertReporter->addWarning($this->translator->trans($message));
    }


    /**
     * @param null $more
     */
    public function formSaved($more = null)
    {
        $this->addSuccess(ConstService::MESSAGE_FORM_SAVED.$more);
    }

    /**
     *
     */
    public function formError()
    {
        $this->addError(ConstService::MESSAGE_FORM_ERROR);
    }

    public function removed()
    {
        $this->addSuccess(ConstService::MESSAGE_MODEL_REMOVED);
    }

    public function systemFailure()
    {
        $this->addWarning(ConstService::MESSAGE_SYSTEM_FAILURE);
    }

    public function formInvalid()
    {
        $this->addWarning(ConstService::MESSAGE_FORM_INVALID);
    }

}