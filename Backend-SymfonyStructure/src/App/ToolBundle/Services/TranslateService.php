<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 28/05/15
 * Time: 09:12
 */

namespace App\ToolBundle\Services;


use App\ModelBundle\Services\Query\Product\ProductDefineCategoryQueryService;
use App\ModelBundle\Services\Query\Product\ProductQueryService;
use Lib\Model\ProductDefineCategoryPeer;

/**
 * Class TranslateService
 * @package App\ToolBundle\Services
 */
class TranslateService
{

    /**
     * @var ProductQueryService
     */
    private $productQueryService;
    /**
     * @var ProductDefineCategoryQueryService
     */
    private $productDefineCategoryQueryService;

    /**
     * @var DatabaseService
     */
    private $databaseService;

    /**
     * @param ProductQueryService $productQueryService
     * @param ProductDefineCategoryQueryService $productDefineCategoryQueryService
     * @param DatabaseService $databaseService
     */
    function __construct(ProductQueryService $productQueryService, ProductDefineCategoryQueryService $productDefineCategoryQueryService, DatabaseService $databaseService)
    {
        $this->productQueryService = $productQueryService;
        $this->productDefineCategoryQueryService = $productDefineCategoryQueryService;
        $this->databaseService=$databaseService;
    }

    /**
     * @return mixed
     */
    public function getObjects()
    {
        $collection = new \PropelObjectCollection();
        $categories = $this->productDefineCategoryQueryService->getQuery()->setLimit(999)->where("name = aliexpress_name")->find();
        return $categories;


    }

    /**
     * @param $all
     * @return bool
     * @throws \Exception
     * @throws \PropelException
     */
    public function saveObject($all)
    {
        $this->databaseService->beginTransaction();

        foreach($all as $k=>$v){
            $k = explode(":", $k);
            switch($k[0]){
                case ProductDefineCategoryPeer::TABLE_NAME:
                    $category = $this->productDefineCategoryQueryService->getQuery()->findOneById($k[1]);
                    $category->setName($v);
                    $category->save();

                    break;
            }

        }
        $this->databaseService->commit();
        return true;

    }
}