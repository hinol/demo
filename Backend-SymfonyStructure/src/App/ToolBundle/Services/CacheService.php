<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 25/05/15
 * Time: 13:03
 */

namespace App\ToolBundle\Services;


use Lsw\MemcacheBundle\Cache\MemcacheInterface;

/**
 * Class CacheService
 * @package App\ToolBundle\Services
 */
class CacheService {


    /**
     * @var MemcacheInterface
     */
    private $memcacheInterface;


    /**
     * @param MemcacheInterface $memcacheInterface
     */
    public function __construct(MemcacheInterface $memcacheInterface){
        $this->memcacheInterface = $memcacheInterface;

    }


    /**
     * @param $key
     * @param $value
     * @param int $expiration
     * @return mixed
     */
    public function set($key, $value, $expiration=0){
        return $this->memcacheInterface->set($key, $value, $expiration);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key){
        return $this->memcacheInterface->get($key);
    }

    /**
     * @param $key
     * @return bool
     */
    public function is($key){
        return (bool)$this->get($key);
    }
}