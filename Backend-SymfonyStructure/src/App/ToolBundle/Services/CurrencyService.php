<?php


namespace App\ToolBundle\Services;


use Buzz\Browser;

/**
 * Class CurrencyService
 * @package App\ToolBundle\Services
 */
class CurrencyService
{


    /**
     * @var Browser
     */
    private $browser;
    /**
     * @var CacheService
     */
    private $cacheService;


    /**
     * @var array
     */
    private $data;


    const YAHOO_URL = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USDEUR%22,%20%22PLNUSD%22,%20%22PLNEUR%22,%20%22PLNGBP%22,%20%22USDPLN%22,%20%22EURPLN%22,%20%22GBPPLN%22)&env=store://datatables.org/alltableswithkeys';//'http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in ("USDEUR", "PLNUSD", "PLNEUR", "PLNGBP", "USDPLN", "EURPLN", "GBPPLN")&env=store://datatables.org/alltableswithkeys';


    const CACHE_NAME = "currency";

    /**
     * @param Browser $browser
     * @param CacheService $cacheService
     */
    public function __construct(Browser $browser, CacheService $cacheService)
    {
        $this->browser = $browser;
        $this->cacheService = $cacheService;
    }


    /**
     * @param $source
     * @param $destination
     */
    public function getCurrency($source, $destination)
    {

        $this->checkData();

        return $this->data[$source][$destination];
    }

    /**
     * @return array
     */
    private function checkData()
    {
        if (null == $this->data) {
            $data = $this->cacheService->get(self::CACHE_NAME);
            if ($data) {
                $this->data = $data;
                return $data;
            }
        }
        if($this->data){
            return false;
        }



        $htmlData = $this->browser->get(self::YAHOO_URL);
        $xml = simplexml_load_string($htmlData->getContent());

        $data = [];
        foreach ($xml->results->rate as $result) {
            $name = explode("/", $result->Name);
            $data[$name[0]][$name[1]] = (float)$result->Rate;
        }

        $this->cacheService->set(self::CACHE_NAME, $data, 555);
        $this->data = $data;

        return $data;
    }


}