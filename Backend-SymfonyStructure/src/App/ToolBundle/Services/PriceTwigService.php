<?php


namespace App\ToolBundle\Services;


class PriceTwigService extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
        );
    }

    public function priceFilter($number, $decimals = 2, $decPoint = ',', $thousandsSep = ' ', $suffix = "zł")
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = $price . ' ' . $suffix;

        return $price;
    }

    public function getName()
    {
        return 'app_extension';
    }
}