<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 27/05/15
 * Time: 13:22
 */

namespace App\ToolBundle\Services;

use PDO;
use Propel\Runtime\Propel;

/**
 * Class DatabaseService
 * @package App\ToolBundle\Services
 */
class DatabaseService
{


    /**
     * @var PDO
     */
    private $con;

    /**
     *
     */
    public function beginTransaction($tableName = 'default')
    {
        $con = Propel::getServiceContainer()->getWriteConnection($tableName);
        $this->con = $con;
        $con->beginTransaction();

    }

    /**
     *
     */
    public function commit()
    {
        $this->con->commit();
    }

    /**
     *
     */
    public function rollback()
    {
        $this->con->rollBack();
    }

    /**
     * @return PDO
     */
    public function getCon()
    {
        return $this->con;
    }
}