<?php


namespace App\ToolBundle\Services;


/**
 * Class ArrayTransformService
 * @package App\ToolBundle\Services
 */
class ArrayTransformService
{


    /**
     * @param $array
     * @return array
     */
    public function makeFlat($array, $join = "_")
    {
        $out = [];
        foreach ($array as $key => $child) {
            if (!is_array($child)) {
                $out[$key] = $child;
                unset($array[$key]);
            }
        }
        $out = array_merge($out, $this->flatten_array($array, $join));
        return $out;
    }


    public function flatten_array($array, $join = "_", $preserve_keys = 2, &$out = array(), &$last_subarray_found = [])
    {

        foreach ($array as $key => $child) {
            if (is_array($child)) {
                $last_subarray_found = $key;
                $out = $this->flatten_array($child, $join, $preserve_keys, $out, $last_subarray_found);
            } elseif ($preserve_keys + is_string($key) > 1) {
                if ($last_subarray_found) {
                    $sfinal_key_value = $last_subarray_found . $join . $key;
                } else {
                    $sfinal_key_value = $key;
                }
                $out[$sfinal_key_value] = $child;
            } else {
                $out[] = $child;
            }
        }

        return $out;
    }
}