<?php


namespace App\ToolBundle\Services;


use Symfony\Component\Finder\Finder;

/**
 * Class BackupService
 * @package App\ToolBundle\Services
 */
class BackupService
{


    /**
     * @var
     */
    private $database_host;
    /**
     * @var
     */
    private $database_port;
    /**
     * @var
     */
    private $database_name;
    /**
     * @var
     */
    private $database_user;
    /**
     * @var
     */
    private $database_password;
    /**
     * @var
     */
    private $data_path;
    /**
     * @var
     */
    private $mysqldump_cmd;
    /**
     * @var
     */
    private $mysql_cmd;

    /**
     * @param $mysql_cmd
     * @param $mysqldump_cmd
     * @param $database_host
     * @param $database_port
     * @param $database_name
     * @param $database_user
     * @param $database_password
     * @param $data_path
     */
    public function __construct(
        $mysql_cmd,
        $mysqldump_cmd,
        $database_host,
        $database_port,
        $database_name,
        $database_user,
        $database_password,
        $data_path
    )
    {
        $this->mysql_cmd = $mysql_cmd;
        $this->mysqldump_cmd = $mysqldump_cmd;
        $this->database_host = $database_host;
        $this->database_name = $database_name;
        $this->database_password = $database_password;
        $this->database_port = $database_port;
        $this->database_user = $database_user;
        $this->data_path = $data_path;
    }

    /**
     * @return Finder
     */
    public function getList()
    {

        $finder = new Finder();
        $finder->files()->in($this->data_path . '/backup');


        if ($finder->count() > 5) {

            $i = 0;
            foreach ($finder as $file) {
                unlink($file->getRealpath());
                $i++;
                if ($i > 10) {
                    return $this->getList();
                }
            }
        }

        return $finder;
    }

    /**
     * @param $name
     */
    public function make($name)
    {


        $name = date("Y-m-d_H-i-s") . '_' . $name;


        if ($this->database_password) {
            $cmd = "%s --opt --lock-all-tables=FALSE --lock-tables=FALSE  -u%s -p%s -h%s  %s  > %s/%s.sql";
            if (!is_dir($this->data_path . '/backup/')) {
                mkdir($this->data_path . '/backup/', 0777, true);
            }

            $cmd = sprintf($cmd,
                $this->mysqldump_cmd,
                $this->database_user,
                $this->database_password,
                $this->database_host,
                $this->database_name,
                $this->data_path . '/backup/',
                $name);

        } else {
            $cmd = "%s --opt --lock-all-tables=FALSE --lock-tables=FALSE  -u%s  -h%s  %s  > %s/%s.sql";
            if (!is_dir($this->data_path . '/backup/')) {
                mkdir($this->data_path . '/backup/', 0777, true);
            }

            $cmd = sprintf($cmd,
                $this->mysqldump_cmd,
                $this->database_user,
                $this->database_host,
                $this->database_name,
                $this->data_path . '/backup/',
                $name);
        }
        exec($cmd);

    }

    /**
     * @param $filePath
     */
    public function restore($filePath)
    {
        if ($this->database_password) {
            $cmd = sprintf("%s -u%s -p%s -h%s %s < %s",
                $this->mysql_cmd,
                $this->database_user,
                $this->database_password,
                $this->database_host,
                $this->database_name,
                $filePath);
        } else {
            $cmd = sprintf("%s -u%s -h%s %s < %s",
                $this->mysql_cmd,
                $this->database_user,

                $this->database_host,
                $this->database_name,
                $filePath);
        }

        exec($cmd);

    }
}