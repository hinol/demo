
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- Bet__Game
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Bet__Game`;

CREATE TABLE `Bet__Game`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `game_type` TINYINT,
    `date` DATETIME,
    `main_win` FLOAT,
    `status` TINYINT,
    `number_status` TINYINT DEFAULT 0,
    `wins_level_status` TINYINT DEFAULT 0,
    `hour` VARCHAR(255),
    `win3` FLOAT,
    `win4` FLOAT,
    `win5` FLOAT,
    `win6` FLOAT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Bet__GameNumber
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Bet__GameNumber`;

CREATE TABLE `Bet__GameNumber`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `game_id` INTEGER NOT NULL,
    `number` INTEGER,
    `mode` TINYINT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `Bet__GameNumber_fi_8b5d72` (`game_id`),
    CONSTRAINT `Bet__GameNumber_fk_8b5d72`
        FOREIGN KEY (`game_id`)
        REFERENCES `Bet__Game` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Bet__Bet
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Bet__Bet`;

CREATE TABLE `Bet__Bet`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `bet_game_id` INTEGER,
    `slug` VARCHAR(50),
    `game_type` TINYINT,
    `system` TINYINT,
    `user_id` INTEGER,
    `shares_cnt` INTEGER,
    `share_price` FLOAT,
    `is_printed` TINYINT(1),
    `moved_cnt` INTEGER,
    `status` TINYINT,
    `lottery_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `Bet__Bet_fi_2c1db7` (`bet_game_id`),
    INDEX `Bet__Bet_fi_f0aebd` (`user_id`),
    CONSTRAINT `Bet__Bet_fk_2c1db7`
        FOREIGN KEY (`bet_game_id`)
        REFERENCES `Bet__Game` (`id`),
    CONSTRAINT `Bet__Bet_fk_f0aebd`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__User` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Bet__BetResult
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Bet__BetResult`;

CREATE TABLE `Bet__BetResult`
(
    `bet_id` INTEGER NOT NULL,
    `win6` FLOAT,
    `win5` FLOAT,
    `win4` FLOAT,
    `win3` FLOAT,
    `win6cnt` FLOAT,
    `win5cnt` FLOAT,
    `win4cnt` FLOAT,
    `win3cnt` FLOAT,
    `win6total` FLOAT,
    `win5total` FLOAT,
    `win4total` FLOAT,
    `win3total` FLOAT,
    `total` FLOAT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0,
    PRIMARY KEY (`bet_id`),
    CONSTRAINT `Bet__BetResult_fk_c729b2`
        FOREIGN KEY (`bet_id`)
        REFERENCES `Bet__Bet` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Bet__BetNumber
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Bet__BetNumber`;

CREATE TABLE `Bet__BetNumber`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `bet_id` INTEGER NOT NULL,
    `number` INTEGER,
    `mode` TINYINT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `Bet__BetNumber_fi_c729b2` (`bet_id`),
    CONSTRAINT `Bet__BetNumber_fk_c729b2`
        FOREIGN KEY (`bet_id`)
        REFERENCES `Bet__Bet` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Bet__BetShare
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Bet__BetShare`;

CREATE TABLE `Bet__BetShare`
(
    `bet_id` INTEGER NOT NULL,
    `user_id` INTEGER NOT NULL,
    `cnt` INTEGER,
    `price` FLOAT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0,
    PRIMARY KEY (`bet_id`,`user_id`),
    INDEX `Bet__BetShare_fi_f0aebd` (`user_id`),
    CONSTRAINT `Bet__BetShare_fk_c729b2`
        FOREIGN KEY (`bet_id`)
        REFERENCES `Bet__Bet` (`id`)
        ON DELETE CASCADE,
    CONSTRAINT `Bet__BetShare_fk_f0aebd`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__User` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Bet__BetWin
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Bet__BetWin`;

CREATE TABLE `Bet__BetWin`
(
    `bet_id` INTEGER NOT NULL,
    `user_id` INTEGER NOT NULL,
    `value` FLOAT,
    `status` TINYINT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`bet_id`,`user_id`),
    INDEX `Bet__BetWin_fi_f0aebd` (`user_id`),
    CONSTRAINT `Bet__BetWin_fk_c729b2`
        FOREIGN KEY (`bet_id`)
        REFERENCES `Bet__Bet` (`id`)
        ON DELETE CASCADE,
    CONSTRAINT `Bet__BetWin_fk_f0aebd`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__User` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Core__File
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Core__File`;

CREATE TABLE `Core__File`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200),
    `source` VARCHAR(600),
    `file_name` VARCHAR(100),
    `file_path` VARCHAR(300),
    `type` TINYINT,
    `filesize` INTEGER,
    `width` INTEGER,
    `height` INTEGER,
    `mimeType` VARCHAR(50),
    `data` TEXT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `source_idx` (`source`),
    INDEX `type_idx` (`type`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Core__Mail
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Core__Mail`;

CREATE TABLE `Core__Mail`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER,
    `name` VARCHAR(200),
    `title` VARCHAR(600),
    `body` VARCHAR(2000),
    `recipient_email` VARCHAR(100),
    `recipient_name` VARCHAR(100),
    `sender_email` VARCHAR(100),
    `sender_name` VARCHAR(100),
    `sent_at` DATE,
    `status` TINYINT,
    `try_cnt` INTEGER,
    `is_read` TINYINT(1),
    `read_at` DATE,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `Core__Mail_fi_f0aebd` (`user_id`),
    CONSTRAINT `Core__Mail_fk_f0aebd`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__User` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Core__Setting
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Core__Setting`;

CREATE TABLE `Core__Setting`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `group_name` VARCHAR(20),
    `name` VARCHAR(50),
    `value` VARCHAR(100),
    `value_type` VARCHAR(20),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `group_name_idx` (`group_name`, `name`),
    INDEX `group_idx` (`group_name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Finance__Transaction
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Finance__Transaction`;

CREATE TABLE `Finance__Transaction`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `operation_type` TINYINT,
    `description` VARCHAR(500),
    `amount` FLOAT,
    `user_amount` FLOAT,
    `status` TINYINT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0,
    PRIMARY KEY (`id`),
    INDEX `Finance__Transaction_fi_f0aebd` (`user_id`),
    CONSTRAINT `Finance__Transaction_fk_f0aebd`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__User` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- User__User
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User__User`;

CREATE TABLE `User__User`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(255),
    `username2` VARCHAR(255),
    `username_canonical` VARCHAR(255),
    `email` VARCHAR(255) NOT NULL,
    `email_canonical` VARCHAR(255),
    `firstname` VARCHAR(255),
    `lastname` VARCHAR(255),
    `phone` VARCHAR(255),
    `enabled` TINYINT(1) DEFAULT 1,
    `salt` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `last_login` DATETIME,
    `locked` TINYINT(1) DEFAULT 0,
    `expired` TINYINT(1) DEFAULT 0,
    `expires_at` DATETIME,
    `confirmation_token` VARCHAR(255),
    `password_requested_at` DATETIME,
    `credentials_expired` TINYINT(1) DEFAULT 0,
    `credentials_expire_at` DATETIME,
    `roles` TEXT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `User__User_u_452367` (`username_canonical`),
    UNIQUE INDEX `User__User_u_a34432` (`email_canonical`),
    UNIQUE INDEX `User__User_u_ce4c89` (`email`),
    UNIQUE INDEX `User__User_u_f86ef3` (`username`),
    UNIQUE INDEX `User__User_u_41ae38` (`phone`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- User__UserData
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User__UserData`;

CREATE TABLE `User__UserData`
(
    `user_id` INTEGER NOT NULL,
    `phone_status` TINYINT,
    `phone_code` VARCHAR(50),
    `phone_code_at` DATETIME,
    `amount` FLOAT,
    `address_street` VARCHAR(200),
    `address_city` VARCHAR(100),
    `address_city_code` VARCHAR(6),
    `bank_number` VARCHAR(50),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0,
    PRIMARY KEY (`user_id`),
    CONSTRAINT `User__UserData_fk_f0aebd`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__User` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- User__Group
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User__Group`;

CREATE TABLE `User__Group`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `roles` TEXT,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- User__UserGroup
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User__UserGroup`;

CREATE TABLE `User__UserGroup`
(
    `user_id` INTEGER NOT NULL,
    `group_id` INTEGER NOT NULL,
    PRIMARY KEY (`user_id`,`group_id`),
    INDEX `User__UserGroup_fi_296699` (`group_id`),
    CONSTRAINT `User__UserGroup_fk_f0aebd`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__User` (`id`)
        ON DELETE CASCADE,
    CONSTRAINT `User__UserGroup_fk_296699`
        FOREIGN KEY (`group_id`)
        REFERENCES `User__Group` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- User__Parameter
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User__Parameter`;

CREATE TABLE `User__Parameter`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `name` VARCHAR(50),
    `value` VARCHAR(500),
    PRIMARY KEY (`id`),
    INDEX `User__Parameter_fi_f0aebd` (`user_id`),
    CONSTRAINT `User__Parameter_fk_f0aebd`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__User` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- User__History
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User__History`;

CREATE TABLE `User__History`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `type` INTEGER,
    `desc` VARCHAR(100),
    `result` INTEGER,
    `amount` FLOAT,
    `total` FLOAT,
    PRIMARY KEY (`id`),
    INDEX `User__History_fi_f0aebd` (`user_id`),
    CONSTRAINT `User__History_fk_f0aebd`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__User` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Core__Setting_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Core__Setting_archive`;

CREATE TABLE `Core__Setting_archive`
(
    `id` INTEGER NOT NULL,
    `group_name` VARCHAR(20),
    `name` VARCHAR(50),
    `value` VARCHAR(100),
    `value_type` VARCHAR(20),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `group_idx` (`group_name`),
    INDEX `Core__Setting_archive_i_61c951` (`group_name`, `name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- User__UserData_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User__UserData_archive`;

CREATE TABLE `User__UserData_archive`
(
    `user_id` INTEGER NOT NULL,
    `phone_status` TINYINT,
    `phone_code` VARCHAR(50),
    `phone_code_at` DATETIME,
    `amount` FLOAT,
    `address_street` VARCHAR(200),
    `address_city` VARCHAR(100),
    `address_city_code` VARCHAR(6),
    `bank_number` VARCHAR(50),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Bet__BetResult_version
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Bet__BetResult_version`;

CREATE TABLE `Bet__BetResult_version`
(
    `bet_id` INTEGER NOT NULL,
    `win6` FLOAT,
    `win5` FLOAT,
    `win4` FLOAT,
    `win3` FLOAT,
    `win6cnt` FLOAT,
    `win5cnt` FLOAT,
    `win4cnt` FLOAT,
    `win3cnt` FLOAT,
    `win6total` FLOAT,
    `win5total` FLOAT,
    `win4total` FLOAT,
    `win3total` FLOAT,
    `total` FLOAT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0 NOT NULL,
    PRIMARY KEY (`bet_id`,`version`),
    CONSTRAINT `Bet__BetResult_version_fk_6fe928`
        FOREIGN KEY (`bet_id`)
        REFERENCES `Bet__BetResult` (`bet_id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Bet__BetShare_version
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Bet__BetShare_version`;

CREATE TABLE `Bet__BetShare_version`
(
    `bet_id` INTEGER NOT NULL,
    `user_id` INTEGER NOT NULL,
    `cnt` INTEGER,
    `price` FLOAT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0 NOT NULL,
    PRIMARY KEY (`bet_id`,`user_id`,`version`),
    CONSTRAINT `Bet__BetShare_version_fk_ca4e37`
        FOREIGN KEY (`bet_id`,`user_id`)
        REFERENCES `Bet__BetShare` (`bet_id`,`user_id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Core__Setting_version
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Core__Setting_version`;

CREATE TABLE `Core__Setting_version`
(
    `id` INTEGER NOT NULL,
    `group_name` VARCHAR(20),
    `name` VARCHAR(50),
    `value` VARCHAR(100),
    `value_type` VARCHAR(20),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0 NOT NULL,
    PRIMARY KEY (`id`,`version`),
    CONSTRAINT `Core__Setting_version_fk_c97a50`
        FOREIGN KEY (`id`)
        REFERENCES `Core__Setting` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Finance__Transaction_version
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Finance__Transaction_version`;

CREATE TABLE `Finance__Transaction_version`
(
    `id` INTEGER NOT NULL,
    `user_id` INTEGER NOT NULL,
    `operation_type` TINYINT,
    `description` VARCHAR(500),
    `amount` FLOAT,
    `user_amount` FLOAT,
    `status` TINYINT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0 NOT NULL,
    PRIMARY KEY (`id`,`version`),
    CONSTRAINT `Finance__Transaction_version_fk_09a994`
        FOREIGN KEY (`id`)
        REFERENCES `Finance__Transaction` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- User__UserData_version
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User__UserData_version`;

CREATE TABLE `User__UserData_version`
(
    `user_id` INTEGER NOT NULL,
    `phone_status` TINYINT,
    `phone_code` VARCHAR(50),
    `phone_code_at` DATETIME,
    `amount` FLOAT,
    `address_street` VARCHAR(200),
    `address_city` VARCHAR(100),
    `address_city_code` VARCHAR(6),
    `bank_number` VARCHAR(50),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0 NOT NULL,
    PRIMARY KEY (`user_id`,`version`),
    CONSTRAINT `User__UserData_version_fk_cf5a20`
        FOREIGN KEY (`user_id`)
        REFERENCES `User__UserData` (`user_id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
