<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new App\ModelBundle\AppModelBundle(),
            new App\ToolBundle\AppToolBundle(),
            new Propel\PropelBundle\PropelBundle(),

            new Sensio\Bundle\BuzzBundle\SensioBuzzBundle(),

            // Add your dependencies
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            //...

            // If you haven't already, add the storage bundle
            // This example uses SonataDoctrineORMAdmin but
            // it works the same with the alternatives
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            new Sonata\PropelAdminBundle\SonataPropelAdminBundle(),


            new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new Sonata\FormatterBundle\SonataFormatterBundle(),


            new Sonata\MediaBundle\SonataMediaBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Lsw\MemcacheBundle\LswMemcacheBundle(),

            new Sonata\IntlBundle\SonataIntlBundle(),

            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\AopBundle\JMSAopBundle(),

            new Bazinga\Bundle\PropelEventDispatcherBundle\BazingaPropelEventDispatcherBundle(),

            new FOS\UserBundle\FOSUserBundle(),
            new Sonata\ClassificationBundle\SonataClassificationBundle(),



            new JMS\TranslationBundle\JMSTranslationBundle(),

            new SPE\FilesizeExtensionBundle\SPEFilesizeExtensionBundle(),


            // Then add SonataAdminBundle
            new Sonata\AdminBundle\SonataAdminBundle(),
            new Backend\AdminBundle\BackendAdminBundle(),
            new Frontend\StaticBundle\FrontendStaticBundle(),
            new Frontend\CoreBundle\FrontendCoreBundle(),
            new Backend\PanelBundle\BackendPanelBundle(),
            new Ras\Bundle\FlashAlertBundle\RasFlashAlertBundle(),
            new Backend\UserBundle\BackendUserBundle(),
            new Frontend\UserBundle\FrontendUserBundle(),
            new Frontend\BetBundle\FrontendBetBundle(),
            new Frontend\FinanceBundle\FrontendFinanceBundle(),
            new Backend\GameBundle\BackendGameBundle(),
            new Backend\BetBundle\BackendBetBundle(),
            new Backend\TestBundle\BackendTestBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
