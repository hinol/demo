# Frontend-jsCode # 

* example js library for Frontend - build by GULP
* tools: bower, nodejs, gulp
* libraries: jquery, bootstrap, 
* features
* * very flexible code to include new libraries js, detail in Main.js
* * generate two files - main.js, main-lib.js - in production - will change it to one file


# Backend-SymfonyStructure # 

* example code of Symfony 2.7, this code based on my project base code to create every new project
* integrated: Propel2, Sonata, SonataMediaBundle, FosUserBundle etc
* features
* * everything in services
* * integrate fos/sonata/propel
* * ready to unit test,api,etc