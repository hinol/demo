var gulp = require('gulp'),
    mainBowerFiles = require('main-bower-files'),
    gulpFilter = require('gulp-filter'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    minifycss = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    flatten = require("gulp-flatten");


var dest_path = './bower_components_prod/';
var prod_path = '../../web/static/';

gulp.task("style", function () {
    gulp.src('./src/less/main.less')
        .pipe(less())
        .pipe(minifycss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(prod_path + '/css'));
});

gulp.task('script-lib', function () {
    return gulp.src([
        "./src/js-lib/jquery.min.js",
        "./src/js-lib/bootstrap.min.js",
        "./src/js-lib/jquery-ui.min.js",
        "./src/js-lib/jquery.cookie.js",
        "./src/js-lib/Main.js",
    ])
        //.pipe(uglify())
        .pipe(concat('main-lib.min.js'))
        .pipe(gulp.dest(prod_path + "/js"));
});
gulp.task('script', function () {
    return gulp.src([

        "./src/js/**/*.js"
    ])
        //.pipe(uglify())
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest(prod_path + "/js"));
});


gulp.task('copyfonts', function () {
    gulp.src('./bower_components/fontawesome/fonts/**/*.{ttf,woff,eof,svg}')
        .pipe(gulp.dest(prod_path + '/fonts'));
});

gulp.task('image', function () {
    gulp.src('./src/images/**/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(prod_path + '/images'));
});

gulp.task('watch', function () {
    gulp.watch("./src/js/**/*", ['script']);
    gulp.watch("./src/less/**/*", ['style']);
    gulp.watch("./src/image/**/*", ['image']);

});


gulp.task('default', ['watch', 'style', 'script', 'copyfonts', 'script-lib', 'image']);