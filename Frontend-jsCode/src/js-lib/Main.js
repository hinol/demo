var Main = {

    modules: [],


    init: function () {
        for (var module in this.modules) {

            module = this.modules[module];
            if (module.object.size() > 0) {
                module.module.init();
            }
        }
    },

    add: function (module, object) {

        this.modules.push({module: module, object: object});
    }
};

$(document).ready(function () {

    Main.init()
});


