var RegisterForm = {


    init: function () {
        var form = $('form[name=frontend_user_register]');
        form.unbind('submit');
        form.submit(function () {
            $.ajax({
                url: form.attr('action'),
                type: 'post',
                data: form.serializeArray(),
                success: function (msg) {
                    if (typeof msg == "object" && msg.result == "success") {
                        $('#RegisterForm').html(msg.html);
                        return;
                    }
                    $('#RegisterForm').html($(msg).find('#RegisterForm').html());
                    RegisterForm.init();
                }
            });
            return false;
        });
    }
};


Main.add(RegisterForm, $('form[name=frontend_user_register]'));