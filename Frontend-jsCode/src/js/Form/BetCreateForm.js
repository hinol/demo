var BetCreateForm = {

    form: $('form[name=frontend_bet]'),


    sharePrices: new Array(),

    autoValid: false,
    init: function () {


        BetCreateForm.form = $('form[name=frontend_bet]');

        this.initAutoLoad();
        this.initButtonSet("game");
        this.initButtonSet("system");
        this.initButtonSet("numbers");


        this.disable("system");
        this.disable("numbers");


        this.initGameSelector();
        this.initSystemSelector();
        this.initNumbersLimit();


        this.initAutoSave();
        this.initShareBuy();
        this.valid();


    },


    valid: function () {
        this.form.submit(function () {
            BetCreateForm.autoValid=true;
            if (BetCreateForm.isValid()) {
                $.cookie("BetCreateForm", null);

                return true;
            }

            return false;
        });
        this.form.find('input').change(function () {

            BetCreateForm.isValid();
        });
    },
    isValid: function () {

        if(this.autoValid != true){
            return false;
        }

        this.form.find('.alert-danger').addClass('hide');
        var error = false;
        if (this.getGame() == null) {
            this.form.find('#frontend_bet_game_type_error').removeClass('hide');
            error = true;
        }

        if (this.getSystem() == null) {
            this.form.find('#frontend_bet_system_error').removeClass('hide');
            error = true;
        }
        if (this.getNumbers() == null || this.getNumbers().length != this.getSystem()) {
            this.form.find('#frontend_bet_numbers_error').removeClass('hide');
            error = true;
        }

        if (this.form.find('#frontend_bet_share_slider').data("max-price") < this.form.find('#frontend_bet_bet_shares_0_price').val()) {
            this.form.find('#frontend_bet_price_error').removeClass('hide');
            error = true;
        }
        if (error) {
            return false;
        }
        return true;


    },

    initAutoLoad: function () {
        var data = $.cookie("BetCreateForm");


        if (typeof data != "undefined") {
            data = data.split(",");
            if (data[0]) {
                BetCreateForm.form.find("input[value=" + data[0] + "]").attr("checked", true);
            }

            if (data[1]) {
                BetCreateForm.form.find("#frontend_bet_system input[value=" + data[1] + "]").attr("checked", true);
                BetCreateForm.enable("numbers");
                BetCreateForm.refreshSharePrice();
            }

            if (data[2] && data[2].length > 0) {
                var numbers = data[2].split(";");
                for (number in numbers) {
                    var number = numbers[number];
                    BetCreateForm.form.find(".frontend_bet_numbers input[value=" + number + "]").attr("checked", true);
                }

            }
            if (data[3]) {
                BetCreateForm.form.find('#frontend_bet_bet_shares_0_cnt').val(data[3]);
            }

        }

    },
    initAutoSave: function () {


        this.form.find('input').change(function () {

            var values = [];
            values[0] = BetCreateForm.getGame();
            values[1] = BetCreateForm.getSystem();
            if (BetCreateForm.getNumbers()) {
                values[2] = BetCreateForm.getNumbers().join(';');
            } else {
                values[2] = null;
            }
            values[3] = BetCreateForm.form.find('#frontend_bet_bet_shares_0_cnt').val();

            $.cookie("BetCreateForm", values, {expires: 7});
        });
    },
    initShareBuy: function () {
        var box = $("#frontend_bet_share_slider");
        box.slider({
            min: 0,
            max: box.data('max-share'),
            value: BetCreateForm.form.find('#frontend_bet_bet_shares_0_cnt').val(),
            slide: function (event, ui) {
                BetCreateForm.setShareCnt(ui.value);
            }
        });
    },
    sharePricesCache: [],
    refreshSharePrice: function () {
        var game = BetCreateForm.getGame();
        var system = BetCreateForm.getSystem();


        if (game == null || system == null) {
            return false;
        }
        var price = this.form.find('#frontend_bet_share_slider').data("prices")[game][system];

        BetCreateForm.sharePrices = price;
        if (BetCreateForm.form.find('#frontend_bet_bet_shares_0_cnt').val()) {
            BetCreateForm.setShareCnt(BetCreateForm.form.find('#frontend_bet_bet_shares_0_cnt').val());
        }
    }
    ,
    setShareCnt: function (cnt) {
        this.form.find('#frontend_bet_bet_shares_0_cnt').val(cnt).change();
        this.form.find('#frontend_bet_bet_shares_0_price').val(BetCreateForm.sharePrices[cnt]);
    }
    ,
    initSystemSelector: function () {
        this.form.find("#frontend_bet_system").unbind('change').change(function () {
            var system = BetCreateForm.getSystem();

            if (system) {
                BetCreateForm.enable("numbers");
                BetCreateForm.initNumbersLimit();
            } else {

                BetCreateForm.disable("numbers");
            }

            var numbers = BetCreateForm.getNumbersAsObject();
            if (numbers) {
                while (system < numbers.size()) {
                    numbers.eq(numbers.size() - 1).click();
                    numbers = BetCreateForm.getNumbersAsObject();
                }
            }
            BetCreateForm.refreshSharePrice();

        });

    }
    ,
    disable: function (name) {
        switch (name) {
            case "system":
                this.form.find('#frontend_bet_system input').attr('disabled', true);
                break;
            case "numbers":
                this.form.find('.frontend_bet_numbers input').attr('disabled', true);
                break;
        }
        BetCreateForm.initButtonSet(name);
    }
    ,
    enable: function (name) {
        switch (name) {
            case "system":
                this.form.find('#frontend_bet_system input').attr('disabled', false);
                break;
            case "numbers":
                this.form.find('.frontend_bet_numbers input').attr('disabled', false);
                break;
        }
        BetCreateForm.initButtonSet(name);
    }
    ,
    initNumbersLimit: function () {
        this.form.find(".frontend_bet_numbers").unbind('change').change(function () {
            var numbers = BetCreateForm.getNumbersAsObject();
            var system = BetCreateForm.getSystem();
            if (numbers) {
                if (numbers.size() == system) {
                    $('.frontend_bet_numbers input').attr('disabled', true);
                    numbers.attr('disabled', false);
                } else {
                    $('.frontend_bet_numbers input').attr('disabled', false);
                }
                numbers.attr('disabled', false);
            }
            BetCreateForm.initButtonSet("numbers");

        });
        if (this.getSystem()) {
            this.form.find(".frontend_bet_numbers").change();
        }

    }
    ,
    initButtonSet: function (name) {
        switch (name) {
            case "numbers":
                this.form.find(".frontend_bet_numbers").buttonset();
                break;
            case "system":
                this.form.find("#frontend_bet_system input").button();
                break;
            case "game":
                this.form.find("#frontend_bet_game_type_0").button();
                this.form.find("#frontend_bet_game_type_1").button();
                break;
            default:
                this.form.find("#frontend_bet_game_type").buttonset();
                this.form.find("#frontend_bet_system").buttonset();
                this.form.find(".frontend_bet_numbers").buttonset();
                break;
        }
    }
    ,
    initGameSelector: function () {
        this.form.find("#frontend_bet_game_type").unbind('change').change(function () {
            var box = BetCreateForm.form.find("div#frontend_bet_system");

            $.ajax({
                url: box.data("url"),
                type: "GET",
                data: "game=" + BetCreateForm.getGame(),
                success: function (prices) {
                    for (var system in prices) {
                        $('#system' + system + " span").html(prices[system]);
                        $('#system' + system).removeClass("hide");
                        BetCreateForm.enable("system");
                        BetCreateForm.refreshSharePrice();
                    }
                }
            })
        });
        if (this.getGame()) {
            this.form.find("#frontend_bet_game_type").change();
        }
    }
    ,
    getGame: function () {
        if (this.form.find('input[name="frontend_bet[game_type]"]:checked').size() > 0) {
            return this.form.find('input[name="frontend_bet[game_type]"]:checked').val();
        }
        return null;
    }
    ,
    getSystem: function () {
        if (this.form.find('input[name="frontend_bet[system]"]:checked').size() > 0) {
            return this.form.find('input[name="frontend_bet[system]"]:checked').val();
        }
        return null;
    }
    ,
    getNumbersAsObject: function () {
        if (this.form.find('input[name="frontend_bet[numbers][]"]:checked').size() > 0) {
            return this.form.find('input[name="frontend_bet[numbers][]"]:checked')
        }
        return null;
    }
    ,
    getNumbers: function () {
        if (this.getNumbersAsObject()) {
            var result = [];
            this.getNumbersAsObject().each(function () {
                result.push($(this).val());
            });
            return result;
        }
        return null;
    }


}


Main.add(BetCreateForm, $('form[name=frontend_bet]'));