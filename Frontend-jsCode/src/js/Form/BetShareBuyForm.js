var BetShareBuyForm = {

    form: $('form[name=frontend_bet_share]'),
    sharePrices: [],
    autoValid: false,

    init: function () {
        var box = $("#frontend_bet_share_slider");
        this.form = $('form[name=frontend_bet_share]');

        BetShareBuyForm.sharePrices = this.form.find('#frontend_bet_share_slider').data("prices");
        box.slider({
            min: 0,
            max: box.data('max-share'),
            value: BetCreateForm.form.find('#frontend_bet_bet_shares_0_cnt').val(),
            slide: function (event, ui) {
                BetShareBuyForm.setShareCnt(ui.value);
            }
        });
        this.valid();
    },

    setShareCnt: function (cnt) {
        this.form.find('#frontend_bet_share_cnt').val(cnt).change();
        this.form.find('#frontend_bet_share_price').val(BetShareBuyForm.sharePrices[cnt]);
        this.form.find('#share-cnt-new').html(cnt);
        this.form.find('#price_selected_bets').html(BetShareBuyForm.sharePrices[cnt]);
        this.form.find('#share-price-new').html(BetShareBuyForm.sharePrices[cnt]);
    },

    valid: function () {
        this.form.submit(function () {
            BetShareBuyForm.autoValid = true;
            if (BetShareBuyForm.isValid()) {
                var form = BetShareBuyForm.form;


                $.ajax({
                    url: form.attr("action"),
                    data: form.serialize(),
                    type: "POST",
                    success: function (msg) {
                        if (msg.status == 'success') {
                            document.location = document.location;
                        }
                    }
                })
                return false;
            }

            return false;
        });
        this.form.find('input').change(function () {

            BetShareBuyForm.isValid();
        });
    },
    isValid: function () {

        if (this.autoValid != true) {
            return false;
        }

        this.form.find('.alert-danger').addClass('hide');
        var error = false;


        if (this.form.find('#frontend_bet_share_slider').data("max-price") < this.form.find('#frontend_bet_share_price').val()) {
            this.form.find('#frontend_bet_price_error').removeClass('hide');
            error = true;
        }
        if (error) {
            return false;
        }
        return true;


    },

}

Main.add(BetShareBuyForm, $('form[name=frontend_bet_share]'));