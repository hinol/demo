var LoginForm = {


    init: function () {
        var form = $('form[name=login_form]');
        form.unbind('submit');
        form.submit(function () {
            $.ajax({
                url: form.attr('action'),
                type: 'post',
                data: form.serializeArray(),
                success: function (msg) {
                    if (typeof msg == "object" && msg.result == "success") {
                        document.location = msg.url;
                        return;
                    }
                    $('#LoginForm').html($(msg).find('#LoginForm').html());
                    LoginForm.init();
                }
            });
            return false;
        });
    }
};


Main.add(LoginForm, $('form[name=login_form]'));