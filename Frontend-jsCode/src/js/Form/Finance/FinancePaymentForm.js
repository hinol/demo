var FinancePaymentForm = {

    form: $('form[name=frontend_finance_payment]'),
    init: function () {

        this.form.find("#frontend_finance_payment_amount input").button().change(function () {
            FinancePaymentForm.recount();
        });
        this.form.find("#frontend_finance_payment_currency input").button().change(function () {
            FinancePaymentForm.recount();
        });

        FinancePaymentForm.recount();


    },
    recount: function () {
        var amount = this.form.find('input[name="frontend_finance_payment[amount]"]:checked').val();
        var currencyField = this.form.find('input[name="frontend_finance_payment[currency]"]:checked');
        var currency = currencyField.val();


        this.form.find('.balance-extra').html(amount);
        this.form.find('.balance-extra-in-currency').html(amount * currency);
        this.form.find('.balance-extra-in-currency-name').html(currencyField.data('name'));


    }


}

Main.add(FinancePaymentForm, $('form[name=frontend_finance_payment]'));