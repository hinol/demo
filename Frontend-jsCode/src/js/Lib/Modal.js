var LibModal = {



    init: function () {

        $('.modal-open').each(function () {
            $(this).click(function () {
                var link = $(this);

                $.ajax({
                    url: link.attr('href'),
                    success: function (msg) {
                        $(msg).appendTo("body");

                        var id = $(msg).attr("id");

                        var objectModal = $('#' + id)
                        objectModal.modal();
                        objectModal.on('hidden.bs.modal', function () {
                            objectModal.remove();
                        });


                    }
                });
                return false;
            });
        });
    }
}


Main.add(LibModal, $('.modal-open'))